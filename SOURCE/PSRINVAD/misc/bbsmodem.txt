----------------------------------------------------------------------
  My first IBM PC in 1994 was a 486 Sx (no FPU) 25 Mhz, 4 MB of RAM,
  ~170 MB HD, MS-DOS 6.00 and Windows 3.1, with a 2400 bps (bits per
  second!) modem. 2400 bits is 300 bytes, so pretty slow!
----------------------------------------------------------------------

P4-INV.ZIP is 107 kb, it's Pascal P4 (DJGPP: PCOM.EXE [UPX], PINT.EXE
[UPX]) bytecode compiler and the necessary files to run ISONASM.PAS
(FIXME.PAS, ISONASM.FIX, P4.BAT) on INVADERS.ASM, so 358 seconds
(6 minutes)!

ALICERUN.ZIP (96 kb or 116 kb unpacked) is Alice Pascal cmdline
bytecode compiler (APIN.EXE [UPX], APRUN.EXE [UPX], APBUILT.SUF,
TURBOLIB.AP). So 322 seconds (5 1/2 minutes) to download. Actually,
if you omit the compiler (APIN.EXE) and add INVA86.AP (2 kb), then
it's only 66 kb and thus 222 seconds (3.7 minutes).

OBC.ZIP is 89 kb (INV.EXE [UPX] and OBX.EXE [UPX] from old 1.4p2 DJGPP
Oxford) plus INVOOPOB.TXT (*.m files), so 296 seconds (5 minutes).

TURBOPAS.ZIP (70 kb or 106 kb unpacked) is TPC.EXE and TURBO.TPL from
TP 5.5. So 235 seconds (4 minutes) to download.

MAWK.ZIP (60 kb or 63 kb unpacked) is MAWK.EXE [UPX] (TC++ build) and
ASMDATA.AWK + INV-TINY.AWK as well as INV-NASM.AWK. So 203 seconds
(3 1/2 minutes) to download.

TP302.ZIP (30 kb or 39 kb unpacked) is TURBO.COM from TP 3.02.
That would take 100 seconds (1 2/3 minutes) to download.

SED15X.ZIP ("hhsed") is 25 kb, so 82 seconds (1 1/3 minutes) to
download.

FIXNASM.ZIP is 12 kb with INVNASM.PAS and (FPC i8086-msdos)
INVNASM.COM [UPX], thus 40 seconds.

FIXWOLF.ZIP (8.5 kb) is INVWOLF.C and INVWOLF.EXE [UPX] (TC++), thus
28 seconds.

NASMSUBC.ZIP (7 kb) is INVNASMS.C + INVNASMS.EXE [UPX] from SubC
[2014]), so 23 seconds.

FIXINV.A86 is only 1.8 kb ZIP'd (or 5.7 kb unpacked), so 7 seconds.
