/* REXX */

signal on notready ; signal on syntax ; signal on novalue

call time 'r'

parse version interp ver ; say ; say interp ver ; say
parse source os . thisfile .
parse arg filename splitprocs

if filename='' | splitprocs='' then do
  say 'USAGE:' '9'x 'myfile.rex proc1 proc2 proc3'
  say '9'x thisfile 'G:\INV-NASM.REX fixwords omit sub' ; say
  exit 1
end

filename=translate(filename,'/','\') ; p=pos('/',filename)
if p > 0 then outfile=delstr(filename,1,lastpos('/',filename))
else outfile=filename
p=pos('.',outfile)
if p > 0 then outfile=delstr(outfile,pos('.',outfile))'.new'
if os \== 'UNIX' then filename=translate(filename,'\','/')
if lines(filename) > 0 then do ; say filename '->' outfile ; say ; end
if interp=='brexx' then do
  say '(bug??) You _MUST_ rename *.NEW to *.REX first!' ; say
end

if lines(filename)=1 then do while lines(filename) \= 0
  call grab
end
else do lines(filename)
  call grab
end

say ; say 'Elapsed:' format(time('e'),,2) 'secs.' ; say
exit

grab:
  line=linein(filename)
  if right(word(line,1),1)=':' then do
    parse var line proc ':' 'procedure' .
    if wordpos(proc,splitprocs) \= 0 then do
      say proc'.rex'
      call lineout proc'.rex',proc':'
      do until left(line,6)='return'
        line=linein(filename)
        if length(line) > 0 then call lineout proc'.rex',line
      end
    end
    else if length(line) > 0 then call writeout line
  end
  else if length(line) > 0 then call writeout line
return

writeout: procedure expose outfile
  call lineout outfile,arg(1)
return

syntax:
notready:
  if interp \== 'brexx' then say condition('c')
  say '*** Error on line #'sigl ':' ; say '"'sourceline(sigl)'"' ; say
return

novalue:
  say '!! NOVALUE !!' sigl ':' sourceline(sigl)
return

/* EOF */
