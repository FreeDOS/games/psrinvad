                PSR Invaders 1.1 (patches by rugxulo)
                -------------------------------------

* backport INVTURBO.PAS to "classic" Pascal dialect (ISO 7185)
  = current INVISO.PAS is pathetic (but works ... barely)

* write a simple one-pass assembler in AWK (!)

* write a converter for GNU as (GAS) syntax

<EOF>
