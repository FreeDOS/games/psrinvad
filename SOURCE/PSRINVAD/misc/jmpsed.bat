@echo off
if not exist %0 %0.bat

for %%a in (invaders.asm jmpshort.sed ignore.exe) do if not exist %%a goto end

if "%AWK%"=="" set AWK=awk
if "%SED%"=="" set SED=sed

::#--- jmpsed.awk begins ---
:: /^;/{ next }
:: / *PROC .*/{ sub(" *PROC .*",":") }
:: /^[A-Z][^:]*:/{ print $1 }
:: /JMP /{ print label;print }
::#--- jmpsed.awk ends ---

%AWK% "/\.awk begins ---$/,/ ends ---$/{sub(\"^:: *\",\"\");print}" %0|%AWK% -f - invaders.asm|%SED% -f jmpshort.sed>j
ren j jmpsed.out
%AWK% "/SHORT /{sum++};END{print sum,\"= 65\"}" jmpsed.out
ignore.exe <jmpsed.out
del jmpsed.out >NUL

:end
if "%SED%"=="sed" set SED=
if "%AWK%"=="awk" set AWK=
