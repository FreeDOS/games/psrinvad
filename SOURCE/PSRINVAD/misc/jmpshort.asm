; JMPSHORT.ASM (using A72) for INVADERS.ASM

LINEMAX equ 150

CR equ 13
LF equ 10

TRAP equ 3
DOS equ 21h
EXIT equ 4Ch
OPENREAD equ 3D00h
CREATEFILE equ 3Ch
CLOSEFILE equ 3Eh
READFILE equ 3Fh
WRITEFILE equ 40h
SEEKFILECUR equ 4201h

org 100h

;=======================================
MAIN:
  call openfiles
  jc BYE
READ:
  call readline
  test ax,ax
  jz CLOSE
  call seekfix
  call adjust
  call writeline
  jmp short READ
CLOSE:
  call closefiles
BYE:
  mov ah,EXIT
  mov al,[err]
  int DOS
;=======================================

openfiles:
  mov ax,OPENREAD
  mov dx,offset oldasm
  int DOS
  jc openfiles_ret
  mov [oldhandle],ax

  mov ah,CREATEFILE
  xor cx,cx
  mov dx,offset newasm
  int DOS
  jc openfiles_ret
  mov [newhandle],ax
openfiles_ret:
  ret

closefiles:
  mov ah,CLOSEFILE
  mov bx,[oldhandle]
  int DOS
  jc closefiles_ret

  mov ah,CLOSEFILE
  mov bx,[newhandle]
  int DOS
  jc closefiles_ret

  mov byte[err],0
closefiles_ret:
  ret

readline:
  mov ah,READFILE
  mov bx,[oldhandle]
  mov cx,LINEMAX
  mov dx,offset line
  int DOS
  jc readline_ret
  mov [bytesread],al
  inc word[linenum]
readline_ret:
  ret

seekfix:
  mov cx,ax
  mov dx,ax
  mov al,LF
  mov di,offset line
  repnz scasb
  xchg cx,dx
  sub cx,dx
  sub cx,2
  mov [linelen],cl
  mov ax,SEEKFILECUR
  mov bx,[oldhandle]
  mov cx,-1
  xor dh,dh
  mov dl,[bytesread]
  sub dl,[linelen]
  sub dl,2
  neg dx
  test dx,dx
  jz seekfix_ret
  int DOS
seekfix_ret:
  ret

writeline:
  cmp byte[linelen],0
  jz writeline_crlf
writeline_file:
  mov ah,WRITEFILE
  mov bx,[newhandle]
  xor ch,ch
  mov cl,[linelen]
  mov dx,offset line
  int DOS
  jc writeline_ret
writeline_crlf:
  mov ah,WRITEFILE
  mov bx,[newhandle]
  mov cx,2
  mov dx,offset crlf
  int DOS
writeline_ret:
  ret

adjust:
  cmp byte[linelen],0
  jz adjust_ret
  cmp byte[line],';'
  jz adjust_ret
  mov al,' '
  xor ch,ch
  mov cl,[linelen]
  mov di,offset line
  repnz scasb
  rep scasb
  dec di
adjust_compare:
  xor ch,ch
  mov cl,[jmpmsg_len]
  mov si,offset jmpmsg
  push di
  rep cmpsb
  pop di
  test cx,cx
  jnz adjust_ret
  lea dx,[di+4]
adjust_jumpnum:
  inc byte[jumpnum]
  mov al,[jumpnum]
  add al,' '
  xor ch,ch
  mov cl,[noshort_len]
  inc cl
  mov di,offset noshort
  repnz scasb
  test cx,cx
  jnz adjust_ret
adjust_copy:
  push dx
  sub dx,offset line
  xor ch,ch
  mov cl,[linelen]
  sub cx,dx
  pop dx
  mov bx,offset line-1
  add bl,[linelen]
  mov si,bx
  add bl,[shortmsg_len]
  mov di,bx
  std
  rep movsb
  cld
  xor ch,ch
  mov cl,[shortmsg_len]
  add byte[linelen],cl
  mov di,dx
  mov si,offset shortmsg
  rep movsb
adjust_ret:
  ret


err db 1
oldasm db 'INVADERS.ASM',0
newasm db 'invaders.jmp',0
crlf db CR,LF

noshort db '#$%&,.134578>DGJMOPQRSTUVWXYZ[\]^_`abcdefy'
noshort_len db $-offset noshort
jmpmsg db 'JMP '
jmpmsg_len db $-offset jmpmsg
shortmsg db 'SHORT '
shortmsg_len db $-offset shortmsg

linenum dw 0
jumpnum db 0

oldhandle dw ?
newhandle dw ?
bytesread db ?
linelen db ?
line ds LINEMAX

; EOF
