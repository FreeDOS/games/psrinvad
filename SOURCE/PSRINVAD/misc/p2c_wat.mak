# OpenWatcom 1.9 wmake
# using modified P2C120S.ZIP sources (circa 1999) from DJGPP /current/v2gnu/

.EXTENSIONS:
.EXTENSIONS: .com .obj .asm .c .pas

PASDIR   = C:\TMP\INVADR11\HLL\PASCAL
INVADERS = C:\ZIPS\ASM\invadr11.zip

CC       = *wcl386
CC16     = *wcl
CFLAGS   = -q -za -ox -w0
COMPACT  = -mc
AS       = *wasmr
ASFLAGS  = -q
LD          = *wlink
LDFLAGS     = op q
LDCOMFLAGS  = $(LDFLAGS) format dos com file
#LD         = warplink
#LDCOMFLAGS = /c

UNZIPPER   = unzip
UNZIPFLAGS = -qjn
REXX       = rexx
P2CFLAGS   = -LTURBO -a -q
VI         = vi
VIFLAGS    = -q -s

MPFLAGS    = -n -m -h -t16 -a35
PROTOFLAGS = $(MPFLAGS) -s1 -i
HDRSFLAGS  = $(MPFLAGS) -s0 -x

PART1 = trans.c stuff.c out.c comment.c lex.c parse.c decl.c
PART2 = expr.c pexpr.c funcs.c dir.c hpmods.c citmods.c
OBJS  = $(PART1:.c=.obj) $(PART2:.c=.obj)
MOST1 = $(PART1:trans.c= )
MOST2 = $(PART2:dir.c= )
MOST  = $(MOST1:.c= ) $(MOST2:.c= )

p2c: p2c.exe .SYMBOLIC

p2c.exe: proto.p2c hdrs.p2c $(PART1) $(PART2) p2crc p2crc.l
# manually set envvar P2C_HOME later or use -H cmdline switch
  $(CC) $(CFLAGS) -c -DHASDUMPS -DP2C_HOME="." trans.c
  $(CC) $(CFLAGS) -c -DCUST1=hpmods -DCUST2=citmods dir.c
  $(CC) $(CFLAGS) -c $(MOST)
  $(CC) $(CFLAGS) -fe=$@ $(OBJS)

p2crc: p2crc.sys
  @copy $< $@ >NUL

p2crc.l:
  @type <<$@
Language Turbo
AnsiC 1
HeaderName "p2c.h"
<< keep
  @copy $@ $*.loc >NUL

makproto.vi: # fopen .c for append
  @type <<$@
/"w"/s/"w"/"a"/
x
<< keep

loadfile.vi:
  @type <<$@
g/mem\(/d
x
<< keep

makproto.exe: makproto.vi makproto.c
  @$(VI) $(VIFLAGS) $<
  @$(CC) $(CFLAGS) $*.c
  @rm -f $*.obj

proto.p2c: makproto.exe $(PART1) $(PART2)
  @makproto $(PROTOFLAGS) $(PART1) -o $@
  @makproto $(PROTOFLAGS) $(PART2) -o $@

hdrs.p2c: makproto.exe $(PART1) $(PART2)
  @makproto $(HDRSFLAGS) $(PART1) -o $@
  @makproto $(HDRSFLAGS) $(PART2) -o $@

INVADERS.ASM: $(INVADERS)
  @$(UNZIPPER) $(UNZIPFLAGS) $< *\$@

.pas.c:
  p2c.exe $(P2CFLAGS) $*.pas >NUL

.asm.obj:
  $(AS) $(ASFLAGS) $*

.obj.com:
  $(LD) $(LDCOMFLAGS) $*
  @dir $@ | find /i "com"
  @echo *************** 9A476DE7 ***************
  crc32 $@

#v===invwat begins===v
invwat.pas: $(PASDIR)\invwat.pas
  @copy $< $@ >NUL

invwat.exe: invwat.c p2clib.c
# default Small model is okay, no heap used
  $(CC16) $(CFLAGS) $<

inv-wat.asm inv-wat.inc: invwat.exe INVADERS.ASM
  invwat.exe
#^===invwat ends===^

#v===inviso begins===v
inviso.pas invtp.dif: $(PASDIR)\inviso.txt
  @$(REXX) $< $@

invtp.pas: inviso.pas invtp.dif
  @ren inviso.pas invtp.* >NUL
  @-patch -f -i invtp.dif

invtp.exe: invtp.c p2clib.c
# needs > 64 kb heap data, so Compact model
  $(CC16) $(CFLAGS) $(COMPACT) $<

invtp2.asm: invtp.exe INVADERS.ASM
  invtp.exe INVADERS.ASM $@
#^===inviso ends===^

#v===invturbo begins===v
invturbo.pas bytename.pas loadfile.pas: $(PASDIR)\invturbo.txt
  @$(REXX) $< $@

loadfile.c: loadfile.vi loadfile.pas
  $(VI) $(VIFLAGS) $<
  p2c.exe $(P2CFLAGS) $*.pas >NUL

invturbo.exe: bytename.c loadfile.c invturbo.c p2clib.c
  $(CC16) $(CFLAGS) $(COMPACT) -fe=$@ $<

inv.asm: invturbo.exe INVADERS.ASM
  invturbo.exe
#^===invturbo ends===^

check: p2c inv-wat.com invtp2.com inv.com .SYMBOLIC

clean: .SYMBOLIC
  rm -f *.obj inv*

# EOF
