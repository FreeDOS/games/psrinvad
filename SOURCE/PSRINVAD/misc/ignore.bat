@echo off
REM ... which jumps to ignore? (i.e. not add "SHORT") ...
for %%a in (invaders.asm jmpshort.sed) do if not exist %%a goto end
if "%SED%"=="" set SED=sed
if "%AWK%"=="" set AWK=awk
echo. | %AWK% "END{printf \" \"}"
%SED% -e "s/ *PROC .*/:/" -f jmpshort.sed invaders.asm|%AWK% "/^;/{next};/JMP /{n++};/JMP /&&!/SHORT/{printf \"%c\",n+32}"
echo. | %AWK% "END{print \" \"}"
echo "#$%&,.134578>DGJMOPQRSTUVWXYZ[\]^_`abcdefy"
if "%AWK%"=="awk" set AWK=
if "%SED%"=="sed" set SED=
:end
