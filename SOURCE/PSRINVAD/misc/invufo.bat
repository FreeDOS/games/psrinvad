@echo off
if "%1"=="" goto end
if "%1"=="-dis" goto dis
if not exist %1 goto end
echo.
echo ... changing UFO score from 100 to 5000 ...
echo.
echo ... (before) ...
call %0 -dis %1
bytefix 00000e2c 64 88 %1
bytefix 00000e2d 00 13 %1
echo ... (after) ...
call %0 -dis %1
echo.
goto end
:dis
ndisasm -b16 -k 0xe2e,0x15bc %2 | grep "^00000E2B"
:end
