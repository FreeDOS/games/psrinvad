{$ifdef FPC} {$mode tp} {$endif}
{$ifdef VER55}{$M 4096,0,655360}{$endif}
{$R-,S-}

{
  sed -e "s/ *PROC .*/:/" -f jmpshort.sed invaders.asm | ignore.exe
}

program ignore; {which jumps to ignore?}
  const jumpnum:byte=0; maxline=131;
  var line:string[maxline];
begin
  while not eof do begin
    readln(line);
    if (line[1] <> ';') and (pos('JMP ',line) <> 0) then begin
      inc(jumpnum);
      if pos('SHORT ',line) = 0 then write(chr(jumpnum+ord(' ')))
    end
  end;
  writeln;
  writeln('#$%&,.134578>DGJMOPQRSTUVWXYZ[\]^_`abcdefy',' (expected)')
end.
