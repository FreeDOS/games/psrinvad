#!/usr/bin/awk -f

BEGIN{
  skip1["ASSUME"]  = skip1["Assume"] = skip1["ORG"]  = skip1["END"]  = \
  skip2["SEGMENT"] = skip2["ENDS"]   = skip2["PROC"] = skip2["ENDP"] = 1
}

/^;/ || /^ *$/ ||
  $1 ~ /^D[BWD]$/ || $2 ~ /^D[BWD]$/ || $1 in skip1 || $2 in skip2 {
    next
}

{
  if ($1 ~ /:$/) { if (NF > 1) insn[$2]++ }
  else if (!($1 in skip1)) insn[$1]++
}

END{
  for (i in insn){
    num++
    if (sortable)
      printf "%3d = %-5s\n",insn[i],i
    else
      printf "%-5s (%3d)\n",i,insn[i]
  }
  print "Total unique instructions used:",num
}

# EOF
