{$ifdef FPC} {$mode tp} {$endif}
{$ifdef VER55}{$M 4096,0,655360}{$endif}
{$ifdef NODEBUG}{$R-,S-}{$else}{$R+,S+}{$endif}

program jmpshort;
  var line:string[131]; mylabel:string[19]; inv:text; p:byte;
  const skip:boolean=false;

  function foundlabel:boolean;
  begin
    p := 1; foundlabel := false; if length(line) = 0 then exit;
    if pos(' PROC ',line) <> 0 then begin
      mylabel := copy(line,1,pos(' ',line)-1);
      foundlabel := true
    end
    else if (line[1] in ['A'..'Z']) then begin
      while (line[p] <> ' ') and (p < length(line)) do inc(p);
      if line[p] = ' ' then dec(p);
      if line[p] = ':' then begin
        mylabel := copy(line,1,p-1);
        foundlabel := true
      end
    end
  end;

  function foundskip:boolean;
  {
    This only works because these labels are sequential and unique
    despite the fact that the first of each pair turns skipping on
      and the second turns skipping off!
  }
    const max=30;
    skiplabels:array [1..max] of string[19]=(
    'DrawBottom2','StartGame',
    'NoMissile','NoPlayerDead',
    'NoRight','Exit',
    'PrintText','NotExit2',
    'NotSound','PrintNext',
    'KeyWasPressed','NotExit',
    'NotSound2','NotExitZ',
    'NotSound3','KillPlayer',
    'AllBombsDead','InvaderBomb',
    'Search2','GotSlot',
    'AllDone1','DrawNextFrameA',
    'AllDone2','NoDeadPlayer',
    'NoFoundSlot','DoneMoveBombs',
    'FindDigit','NotN',
    'Shifting1','NoRow5Kill'
    );
    var n:1..max;
  begin foundskip := false;
    for n := 1 to max do
      if mylabel = skiplabels[n] then begin
        foundskip := true;
        exit
      end
  end;

  procedure sub(older,newer:string);
  begin p := pos(older,line);
    if p <> 0 then begin
      delete(line,p,length(older));
      insert(newer,line,p)
    end
  end;

begin {main}
  assign(inv,'INVADERS.ASM'); reset(inv);
  while not eof(inv) do begin
    readln(inv,line);
    if foundlabel and foundskip then skip := not skip;
    if (line[1] <> ';') and (not skip) then sub('JMP ','JMP SHORT ');
    writeln(line)
  end;
  close(inv)
end.
