@echo off
if "%1"=="-dis" goto dis
if "%SED%"=="" set SED=sed

:begin
if not "%1"=="" if exist %1 goto ndis
for %%a in (inv*.com) do call %0 -dis %%a
goto bye

:dis
shift
if not exist %1 goto end
:ndis
set D1=__dis__.tmp
ndisasm -b16 -o100h %1 | %SED% -e "s/^\( *[^ ][^ ]* *\)[^ ][^ ]* *\(.*\)/\1\2/" >%TEMP%.\%D1%
crc32 %TEMP%.\%D1% | %SED% -e "s/^[^ ][^ ]*[ ][ ]*\(.*\)/\1 %1/"
del %TEMP%.\%D1% >NUL
set D1=
goto end

:bye
if "%SED%"=="sed" set SED=

:end
