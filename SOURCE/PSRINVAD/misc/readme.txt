
Where To Start?
===============

  ...if you just want to rebuild the game (PSR Invaders 1.1)...

$sudo apt-get install nasm

$export URL1=ftp.lanet.lv/ftp/mirror/x2ftp/msdos/programming/gamesrc
$export URL2=www.ibiblio.org/pub/micro/pc-stuff/freedos/files/games/invaders/1.1

$wget $URL1/invadr11.zip -O invadr11.zip
  ...or...
$curl -s -k -L $URL2/invadr11.zip -o invadr11.zip

  ...but you probably already have the .ASM source (packaged by FreeDOS)...

$unzip -CjL invadr11.zip '*/inv-nasm.awk'
$unzip -paa invadr11.zip '*.ASM' | awk -f inv-nasm.awk

$nasm inv-nasm.asm -o inv-nasm.com && rm -f inv-nasm.[ai]*

  ...verify the CRC32 of the .COM output (should say 0xFFF22EF9)...

$7z h inv-nasm.com

  ...now run it, and have fun!...

$dosbox inv-nasm.com -exit &


Why So Much Bloat?
==================

Okay, I overdid it in the patches area because I wanted to not rely on
specific tools like Sed or AWK (or even specific assemblers like
TASM). So I added several other scripts in HLLs (C, BASIC, Pascal,
etc) just to fill that void. It's silly and redundant, but I had fun
anyways. (As best as possible, I stuck to "standard" code that works
with several compilers and targets various assemblers. That was harder
than it sounds!)

<EOF>
