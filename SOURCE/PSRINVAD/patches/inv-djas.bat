@echo off

::#--- fix1.sed begins ---
:: /"/!y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
:: s/\([0-9][0-9a-fA-F]*\)h/0x\1/g
:: s/org/.&/
:: s/ dd /  dw  0,/
:: / [dD][bBwW] /{
:: h
:: s/^\([a-z][^ ]*\) *d\(.\) .*/s,s_\1\\[,\2[,/
:: /,[bw]\[,/w fix2.sed
:: g
:: /"/{
:: s/".*//
:: y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
:: x
:: s/[^"]*\(.*\)/\1/
:: x
:: G
:: s/\n//
:: }
:: s/ \(d[bw]\) /.\1/
:: s/^[a-z][^ ]*/&:\n/
:: b
:: }
:: /^;/b
:: s/word ptr //
:: s/^\([a-z][^:]*\):/\1:\n/
:: /lea /{
:: s//mov /
:: b
:: }
:: /code_seg/d
:: /endp/d
:: /end *begin/d
:: s/ *proc .*/:\n/
:: s/40://
:: s/,09/,9/
:: s/\[0\]//
:: /,of[^ ]*/{
:: s//,/
:: b
:: }
:: s/\[\(.*\)[+]bx\]/s_\1[bx+\1]/
:: s/\([a-z][a-z][a-z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: s/\([a-z][a-z][a-z][a-z0-9]*\),/s_\1[\1],/
:: s/,\([a-z][a-z][a-z][a-z0-9]*\)/,s_\1[\1]/
:: /[+]/!s/\([id].c\)[ ][ ]*\([a-z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: /removenewint9:/,/clc/s/\[\([^0]\)/[cs:\1/
::#--- fix1.sed ends ---

::#--- fix3.sed begins ---
:: s/ *\([bw]\[\)/\1/
:: /,[abcd][hlx]/s/[bw]\[/[/
:: / [abcd][hlx],/s/[bw]\[/[/
:: /,[es][si]/s/w\[/[/
:: / si,/s/w\[/[/
:: s/test.*al,\([^ ]*\)/.db 0xA8,\1 ;&/
:: s/test *ax,\([^ ]*\)/.db 0xA9 ;&\n.dw \1/
:: s/,0x0*\([0-9]\)$/,\1/
:: /,[0-9]$/s/cmpw\[\(.*\)\],/.db 0x83,0x3e\n.dw \1\n.db /
:: /,[0-9][0-9]$/s/cmpw\[\(.*\)\],/.db 0x83,0x3e\n.dw \1\n.db /
:: /,[0-9]$/s/subw\[\(.*\)\],/.db 0x83,0x2e\n.dw \1\n.db /
:: /,[0-9][0-9]*$/s/addw\[\(.*\)\],/.db 0x83,6\n.dw \1\n.db /
::#--- fix3.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised

echo.
echo %%SED%% = '%SED%'

for %%a in (fix1.sed fix3.sed) do %SED% -n -e "/%%a begins ---/,/%%a ends ---$/s/^::[ ][ ]*//w %%a" %0
for %%a in (1 3) do if not exist fix%%a.sed goto end
%SED% -f fix1.sed invaders.asm | %SED% -f fix2.sed -f fix3.sed >inv-djas.as~
redir -eo djasm inv-djas.as~ | %SED% -n -e "s/.*:\([0-9][0-9]*\):.*/\1s,jmp ,jmpl ,/p" >fix4.sed
%SED% -f fix4.sed inv-djas.as~ >inv-djas.asm
del inv-djas.as~ >NUL

djasm inv-djas.asm inv-djas.com
if not exist inv-djas.com goto end

echo.
echo INV-DJAS.COM    FFF22EF9
crc32 inv-djas.com
echo.

if "%1"=="notclean" goto end
del fix?.sed >NUL
del inv-djas.asm >NUL

:end
if "%SED%"=="minised" set SED=
