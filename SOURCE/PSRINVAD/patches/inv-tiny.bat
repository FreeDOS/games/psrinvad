@echo off

::#--- fix1.sed begins ---
:: s/\([0-9][0-9a-fA-F]*\)h/0x\1/g
:: s/ DD / DW 0,/
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/s,s_\1\\[,[\2,/
:: s/,\[B,/,byte[,/
:: s/,\[W,/,word[,/
:: /,[bw]...\[,/w fix2.sed
:: g
:: s/^[A-Z][^ ]*/&:/
:: b
:: }
:: /^;/b
:: /LEA /{
:: s//MOV /
:: b
:: }
:: /CODE_SEG/d
:: /END/d
:: s/ *PROC .*/:/
:: s/40://
:: s/\[0\]//
:: /,O[fF][^ ]*/{
:: s//,/
:: b
:: }
:: s/Word Ptr //
:: s/\[\(.*\)[+]BX\]/s_\1[BX+\1]/
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: /MOV[^;]*[, ]A[LX]/s/s_.*\[/[/
:: /RemoveNewInt9:/,/CLC/s/\[[^0]/CS:&/
:: s/\(.*\)\([CE]S\):/\2 \1/
::#--- fix1.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised

echo.
echo %%SED%% = '%SED%'

%SED% -n -e "/\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix1.sed" %0
if not exist fix1.sed goto end
%SED% -f fix1.sed invaders.asm >inv-tiny.as~
%SED% -f fix2.sed inv-tiny.as~ >inv-tiny.asm
del inv-tiny.as~ >NUL

tinyasm inv-tiny.asm -o inv-tiny.com
if not exist inv-tiny.com goto end

echo.
echo INV-TINY.COM    FFF22EF9
crc32 inv-tiny.com
echo.

if "%1"=="notclean" goto end
del fix?.sed >NUL
del inv-tiny.asm >NUL

:end
if "%SED%"=="minised" set SED=
