@echo off

::#--- fix.sed begins ---
:: 1i\
:: OFFSET equ\
:: Offset equ\
:: LEA equ MOV
:: /^;/b
:: s/ DD / DW 0,/
:: / D[BW] /b
:: /LEA /b
:: /CODE_SEG/d
:: /END/d
:: s/ *PROC .*/:/
:: s/ES:\[/[ES:/
:: s/40://
:: s/\[0\]//
:: /,O/b
:: s/Word Ptr //
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/[\1+\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,[\1]/
:: s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 [\2]/
:: /RemoveNewInt9:/,/CLC/s/\[\([^0]\)/[cs:\1/
::#--- fix.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised
echo %%SED%% = '%SED%'

%SED% -n -e "/fix\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv-fasm.asm

%DPMION%
fasm inv-fasm.asm inv-fasm.com >NUL
%DPMIOFF%
if not exist inv-fasm.com goto end

echo.
echo INV-FASM.COM    FFF22EF9
crc32 inv-fasm.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del inv-fasm.asm >NUL

:end
if "%SED%"=="minised" set SED=
