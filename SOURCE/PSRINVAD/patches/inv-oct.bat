@echo off
REM csed buggy, fix is easy but inefficient

::#--- fix.sed begins ---
:: 1{
:: h
:: s/..*/LANG OCTASM,0.1/w inv-oct.inc
:: g
:: i\
:: LANG OCTASM,0.1 use16 file_out \\inv-oct.com\
:: define eq define\
:: eq AX ax\
:: eq AH ah\
:: eq AL al\
:: eq BX bx\
:: eq CX cx\
:: eq CH ch\
:: eq CL cl\
:: eq DX dx\
:: eq DH dh\
:: eq DL dl\
:: eq SI si\
:: eq DI di\
:: eq CS cs\
:: eq DS ds\
:: eq ES es\
:: eq SEGCS db 2Eh\
:: eq SEGES db 26h\
:: eq movtoAL() db 0A0h dw #1\
:: eq movALto() db 0A2h dw #1\
:: eq movtoAX() db 0A1h dw #1\
:: eq movAXto() db 0A3h dw #1\
:: \\inv-oct.inc
:: }
:: /^;/b
:: s/,O[fF][^ ]*/,/
:: s/\[0\]//
:: /CODE_SEG/d
:: /END/d
:: s/\t/ /g
:: s/ DD / DW 0,/
:: / D[BW] /{
:: s/ DB / db /
:: s/ DW / dw /
:: y/"/'/
:: h
:: s/^\([A-Z][^ ]*\) *d\(.\) .*/eq s_\1 \2/w inv-oct.inc
:: g
:: s/^[A-Z]/#&/
:: b
:: }
:: /^[A-Z].*:[ ][ ]*[A-Z][A-Z]*/{
:: h
:: s/^[A-Z].*:\([ ][ ]*[A-Z][A-Z]*\).*/\1/
:: y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
:: x
:: G
:: s/^\([A-Z].*:\)[ ][ ]*[A-Z][A-Z]*\(.*\)\n\(.*\)/\1\3\2/
:: }
:: /^[ ][ ]*[A-Z][A-Z]*/{
:: h
:: s///
:: x
:: s/^\([ ][ ]*[A-Z][A-Z]*\).*/\1/
:: y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
:: G
:: s/\n//
:: }
:: s/ *PROC .*/:/
:: s/^\([A-Z].*\):/#\1/
:: s/Word Ptr //
:: /lea /{
:: s//mov /
:: b
:: }
:: s/\[\(.*\)[+]BX/s_\1&/
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: s/\([id].c\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: s/40://
:: /#RemoveNewInt9/,/clc/s/\[[^0]/CS:&/
:: s/\(.*\)\([CE]S\):\(\[.*\)/SEG\2 \1\3/
:: /mov /!b
:: /[+]BX/b
:: s/mov *A\([LX]\),.*\[\([^ ][^ ][^ ][^ ]*\)\]/movtoA\1(\2)/
:: s/mov .*\[\([^ ][^ ][^ ][^ ]*\)\],A\([LX]\)/movA\2to(\1)/
:: /BX,/s/,00h.*/,0/
:: /DI,/s/,0 *;.*/,0/
:: s/mov *AX,0$/db 0B8h,0,0 ;&/
:: s/mov *BX,0$/db 0BBh,0,0 ;&/
:: s/mov *DI,0$/db 0BFh,0,0 ;&/
::#--- fix.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised
if "%OCTASM%"=="" set OCTASM=octasm

echo.
echo %%SED%%     = '%SED%'
echo %%OCTASM%%  = '%OCTASM%'
echo.

%SED% -n -e "/fix\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv-oct.asm

%OCTASM% \inv-oct.asm
if not exist inv-oct.com goto end

echo.
echo.
echo INV-OCT.COM     FFF22EF9
crc32 inv-oct.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del inv-oct.inc >NUL
del inv-oct.asm >NUL

:end
if "%OCTASM%"=="octasm" set OCTASM=
if "%SED%"=="minised" set SED=
