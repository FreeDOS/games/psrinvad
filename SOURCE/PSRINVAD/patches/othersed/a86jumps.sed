# manually shorten forward jumps of INV.A86
/^;/b
/^DrawBottom2:/,/^Exit:/b ok
/^ClearLoop3:/,/^NotExit2:/b ok
/^NotSound:/,/^GetKey:/b ok
/^KeyWasPressed:/,/^Search3:/b ok
/^NoFoundSlot1:/,/^LoopingZ:/b ok
/^Search2:/,/^GotSlot:/b ok
/^AllDone1:/,/^DrawNextFrameA:/b ok
/^AllDone2:/,/^NoDeadPlayer:/b ok
/^NoFoundSlot:/,/^DoneMoveBombs:/b ok
/^Done:/,/^NotN:/b ok
/^Shifting1:/,/^NoRow5Kill:/b ok
s/JMP /& SHORT /
:ok
