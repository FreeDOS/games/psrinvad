# manually shorten backward jumps of INV-NASM.ASM
/^;/b
/^BlitAll3:/,/^DonePrinting:/b jmp
/^KillPlayer:/,/^LifeLeft:/b jmp
/^NoFoundSlot1:/,/^AllBombsDead:/{
/^AllBombsDead:/b up
b jmp
}
/^NotExitZ:/,/^NotSound3:/{
/^NotSound3:/b up
b jmp
}
/^NotExit:/,/^NotSound2:/{
/^NotSound2:/b up
b jmp
}
b up
:jmp
s/JMP /& SHORT /
:up
