# manually shorten jumps of INVADERS.ASM
/^DrawBottom2:/,/^StartGame:/b
/^NoMissile:/,/^NoPlayerDead:/b
/^NoRight:/,/^Exit:/b
/^PrintText:/,/^NotExit2:/b
/^NotSound:/,/^PrintNext:/b
/^KeyWasPressed:/,/^NotExit:/b
/^NotSound2:/,/^NotExitZ:/b
/^NotSound3:/,/^KillPlayer:/b
/^AllBombsDead:/,/^InvaderBomb:/b
/^Search2:/,/^GotSlot:/b
/^AllDone1:/,/^DrawNextFrameA:/b
/^AllDone2:/,/^NoDeadPlayer:/b
/^NoFoundSlot:/,/^DoneMoveBombs:/b
/^FindDigit:/,/^NotN:/b
/^Shifting1:/,/^NoRow5Kill:/b
/^;/b
s/JMP /&SHORT /
