# manually shorten signed bytes of INV-NASM.ASM
/^;/b
/[CSA][MUD][PBD] /s/,0*\([0-9]\)h*$/,\1/
/CMP .*,[0-9]$/s/,/,byte /
/CMP .*,[0-9][0-9]$/s/,/,byte /
/SUB .*,[0-9]$/s/,/,byte /
/SUB .*,[0-9][0-9]$/s/,/,byte /
/ADD .*,[0-9]$/s/,/,byte /
/ADD .*,[0-9][0-9]$/s/,/,byte /
