@echo off
if "%SED%"=="" set SED=sed
if "%1"=="-check" goto check
for %%a in (wasm wolf) do if "%1"=="%%a" goto %%a
for %%a in (nasm nasm97) do if "%1"=="%%a" goto %%a

echo.
echo %0 -- faster manual .SED optimizations --
echo.
echo %%SED%% = '%SED%'
echo.
echo Usage:  %0 asm (where 'asm' is one of: [wolf nasm97])
echo.
goto end

:wasm
:wolf
copy a86jumps.sed j1.* >NUL
copy backjump.sed j2.* >NUL
%SED% -f fixwolf.sed -e "/;:/s//:&/" -f j1.sed -f j2.sed -e "/:;:/s///" -e "/ *SHORT/s//S/" invaders.asm >inv-wolf.asm
wasm.com inv-wolf >NUL
call %0 -check inv-wolf.com
del j?.sed inv-wolf.* >NUL
goto end

:nasm
:nasm97
%SED% -f fixnasm.sed -f nasmbyte.sed -f jmpshort.sed invaders.asm >inv-nasm.asm
nasm97 -o inv-nasm.com inv-nasm.asm
call %0 -check inv-nasm.com
del inv-nasm.* >NUL
goto end

:check
shift
if not exist %1 goto bye
dir %1 | find /i "com"
crc32 %1

:end
if "%SED%"=="sed" set SED=
:bye
