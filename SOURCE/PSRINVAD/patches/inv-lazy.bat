@echo off

::#--- fix.sed begins ---
:: 1i\
:: ideal\
:: ifdef @lzasm\
:: option procalign:1\
:: endif
:: /^;/b
:: / D[BW] /b
:: /CS:/b
:: s/\[0\]//
:: /,O[fF]/b
:: s/ DD / DW 0,/
:: s/\(.*\)\(SEGMENT\)$/\2 \1/
:: s/\(.*\)\(END[PS]\)$/\2 \1/
:: s/\(.*\)\(PROC\) /\2 \1 /
:: s/Word Ptr //
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/[\1+\2/
:: /LEA /!{
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9_]*\),/[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9_]*\)/,[\1]/
:: }
:: s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 [\2]/
::#...optional (shuts up warnings)...
:: s/ES:\[/[ES:/
:: /LEA /s/,\([^; ]*\)/,[\1]/
:: s/40://
::#--- fix.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised
if not "%OS%"=="Windows_NT" if "%LAZY%"=="" set LAZY=lzasmx
if "%OS%"=="Windows_NT" if "%LAZY%"=="" set LAZY=lzasm
echo %%SED%%  = '%SED%'
echo %%LAZY%% = '%LAZY%'

%SED% -n -e "/fix\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv-lazy.asm

%LAZY% /t /w0 inv-lazy
if not exist inv-lazy.obj goto end
wlink op q format dos com file inv-lazy
if not exist inv-lazy.com warplink /c inv-lazy >NUL
if not exist inv-lazy.com goto end

echo.
echo INV-LAZY.COM    9A476DE7
crc32 inv-lazy.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del inv-lazy.obj >NUL
del inv-lazy.asm >NUL

:end
if not "%OS%"=="Windows_NT" if "%LAZY%"=="lzasmx" set LAZY=
if "%OS%"=="Windows_NT" if "%LAZY%"=="lzasm" set LAZY=
if "%SED%"=="minised" set SED=
