@echo off

::#--- fix.sed begins ---
:: /^;/b
:: /\[0\]$/s///
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/EXTRN \1:\2/w inv.inc
:: g
:: b
:: }
:: /BEGIN:/s/$/include inv.inc/
:: /^RemoveNewInt9/,/CLC$/{
:: /,Word/s/^/cs:/
:: /,[01]$/s/^/cs:/
:: /StoreAX/s/^/cs:/
:: s/40://
:: }
::#--- fix.sed ends ---

if not exist %0 %0.bat %1
if "%SED%"=="" set SED=minised
if "%ASM86%"=="" set ASM86=a86
REM set A86=+P0
if not exist invaders.asm goto end
echo %%SED%%   = '%SED%'
echo %%ASM86%% = '%ASM86%'
if not "%A86%"=="" echo %%A86%%   = '%A86%'
echo.
%SED% -n -e "/\.sed begins ---$/,/\.sed ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv.a86
if not exist inv.a86 goto cleanup
if exist a86jumps.bat set INVLST=inv-a86.lst
%ASM86% +S inv.a86 inv-a86 %INVLST% >NUL
if not exist inv-a86.com goto cleanup
dir inv-a86.com | %SED% -e "/[cC][oO][mM]/!d"
set CKSUM=F9A3317C
if "%INVLST%"=="" goto checksum
if not exist %INVLST% goto checksum
set CKSUM=CDFE86FA
call a86jumps.bat inv inv-a86
if exist inv-a86.com call a86jumps.bat inv inv-a86
if exist %INVLST% del %INVLST% >NUL
:checksum
echo.
echo INV-A86.COM     %CKSUM%
crc32 inv-a86.com
echo.
set CKSUM=
:cleanup
set INVLST=
if "%1"=="notclean" goto end
del inv.a86 >NUL
if exist inv.inc del inv.inc >NUL
del fix.sed >NUL
:end
if "%ASM86%"=="a86" set ASM86=
if "%SED%"=="minised" set SED=
