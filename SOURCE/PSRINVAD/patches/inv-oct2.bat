@echo off
REM csed buggy, fix is easy but inefficient

::#--- fix.sed begins ---
:: 1{
:: h
:: s/..*/LANG OCTASM,0.1/w inv-oct2.asm
:: g
:: i\
:: LANG OCTASM,0.1 use16 file_out \\inv-oct.com\
:: define eq define\
:: eq SEGcs db 2Eh\
:: eq SEGes db 26h\
:: eq movtoal() db 0A0h dw #1\
:: eq movalto() db 0A2h dw #1\
:: eq movtoax() db 0A1h dw #1\
:: eq movaxto() db 0A3h dw #1\
:: \\inv-oct2.asm
:: }
:: /^;/b
:: s/\t/ /g
:: s/,O[fF][^ ]*/,/
:: s/\[0\]//
:: /CODE_SEG/d
:: /END/d
:: s/ *PROC .*/:/
:: s/ DD / dw 0,/
:: /"/{
:: h
:: s/.*\(".*",0\).*/\1/
:: x
:: s/\([^"]*\)".*/\1/
:: y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
:: G
:: s/\n//
:: }
:: /"/!y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
:: y/"/'/
:: / d[bw] /{
:: h
:: s/^\([a-z][^ ]*\) *d\(.\) .*/eq s_\1 \2/w inv-oct2.asm
:: g
:: s/^[a-z]/#&/
:: b
:: }
:: s/^\([a-z].*\):/#\1/
:: s/word ptr //
:: /lea /{
:: s//mov /
:: b
:: }
:: s/\[\(.*\)[+]bx/s_\1&/
:: s/\([a-z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: s/\([a-z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: s/,\([a-z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: s/\([id][ne]c\)   *\([a-z][a-z][^ ][^ ]*\)/\1 s_\2[\2]/
:: s/40://
:: /#removenewint9/,/clc/s/\[[^0]/cs:&/
:: s/\(.*\)\([ce]s\):\(\[.*\)/SEG\2 \1\3/
:: /mov /!b
:: /[+]bx/b
:: s/mov *a\([lx]\),.*\[\([^ ][^ ][^ ][^ ]*\)\]/movtoa\1(\2)/
:: s/mov .*\[\([^ ][^ ][^ ][^ ]*\)\],a\([lx]\)/mova\2to(\1)/
:: /bx,/s/,00h.*/,0/
:: /di,/s/,0 *;.*/,0/
:: s/mov *ax,0$/db 0B8h,0,0 ;&/
:: s/mov *bx,0$/db 0BBh,0,0 ;&/
:: s/mov *di,0$/db 0BFh,0,0 ;&/
::#--- fix.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised
if "%OCTASM%"=="" set OCTASM=octasm

echo.
echo %%SED%%     = '%SED%'
echo %%OCTASM%%  = '%OCTASM%'
echo.

%SED% -n -e "/fix\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv-oct.asm

%OCTASM% \inv-oct.asm
if not exist inv-oct.com goto end

echo.
echo.
echo INV-OCT.COM     FFF22EF9
crc32 inv-oct.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del inv-oct?.asm >NUL

:end
if "%OCTASM%"=="octasm" set OCTASM=
if "%SED%"=="minised" set SED=
