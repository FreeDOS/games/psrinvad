@echo off

::#--- fix1.sed begins ---
:: /^;/b
:: /CODE_SEG/d
:: s/Exit/&s/
:: s/ DD / DW 0,/
:: / D[BW] /{
:: h
:: s,^\([A-Z][^ ]*\) *D\(.\) .*,s/s_\1\\[/\2[/,w fix2.sed
:: g
:: b
:: }
:: /LEA /{
:: s//MOV /
:: b
:: }
:: s/\[0\]//
:: /,O/b
:: s/Word Ptr //
:: s/ *PROC .*/:/
:: s/40://
:: /[+]BX/s/\[\(.*\)[+]BX/s_\1&/
:: /\[[1-9]\]/s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: /,/s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: /,/s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: /[ID][NE]C/s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: /RemoveNewInt9:/,/CLC/s/\[\([^0]\)/[cs:\1/
::#--- fix1.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised

echo.
echo %%SED%% = '%SED%'
echo.

%SED% -n -e "/fix1\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix1.sed" %0
if not exist fix1.sed goto end

%SED% -f fix1.sed invaders.asm | %SED% -f fix2.sed >inv-tma.asm

tma inv-tma.asm
if not exist inv-tma.com goto end

echo.
echo INV-TMA.COM     6C0F5986
crc32 inv-tma.com
echo.

if "%1"=="notclean" goto end
del fix?.sed >NUL
del inv-tma.asm >NUL

:end
if "%SED%"=="minised" set SED=
