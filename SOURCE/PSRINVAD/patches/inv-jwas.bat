@echo off

::#--- fix.sed begins ---
:: 1i\
:: option noscoped,nokeyword:<lea>\
:: lea macro a,b\
:: mov a,offset b\
:: endm
:: /^;/b
:: /40:/s//ds:/
::#--- fix.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=sed
if not "%OS%"=="Windows_NT" if "%JWATASM%"=="" set JWATASM=jwasmr
if "%JWATASM%"=="" set JWATASM=jwasm
set J1=inv-jwas

echo.
echo %%SED%%     = '%SED%'
echo %%JWATASM%% = '%JWATASM%'
if not "%JWASM%"=="" echo %%JWASM%%   = '%JWASM%'

%SED% -n -e "/\.sed begins/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end

%SED% -f fix.sed invaders.asm >%J1%.asm
%JWATASM% -q -bin -Fo=%J1%.com %J1%.asm
if not exist %J1%.com goto end

echo.
echo %J1%.com    9A476DE7
crc32 %J1%.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del %J1%.asm >NUL

:end
set J1=
if not "%OS%"=="Windows_NT" if "%JWATASM%"=="jwasmr" set JWATASM=
if "%JWATASM%"=="jwasm" set JWATASM=
if "%SED%"=="sed" set SED=
