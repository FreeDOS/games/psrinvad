@echo off

::#--- fix1.sed begins ---
::# sequences unsupported by Minised or Sedmod
::#/.\{126,\}/s/;.*//
:: /^;---/d
:: s/- Causes problems.*//
::
::#s/OUT *\([0-9]*h\)/db 0E6h,\1;&/
:: s/ DD / DW 0,/
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/s,s_\1\\[,[\2,/
:: s/,\[B,/,byte[,/
:: s/,\[W,/,word[,/
:: /,[bw]...\[,/w fix2.sed
:: g
:: b
:: }
:: /^;/b
:: /LEA /{
:: s//MOV /
:: b
:: }
:: /CODE_SEG/d
:: /END/d
:: s/ *PROC .*/:/
:: s/40://
:: s/\[0\]//
:: /,O/b
:: s/Word Ptr //
:: /[+]BX/s/\[\(.*\)[+]BX/s_\1&/
:: /\[[1-9]\]/s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: /,/s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: /,/s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: /[ID][NE]C/s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: /^RemoveNewInt9:/,/CLC$/s/\[[^0]/CS:&/
:: s/\(.*\)\([CE]S\):/\2:\n\1/
::#--- fix1.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised

echo.
echo %%SED%% = '%SED%'

%SED% -n -e "/\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix1.sed" %0
if not exist fix1.sed goto end
if exist jmpshort.sed set JMPS=-f jmpshort.sed
%SED% -f fix1.sed invaders.asm | %SED% -f fix2.sed %JMPS% >inv-a72.asm

set CKSUM=58DF4E20
if not "%JMPS%"=="" set CKSUM=9A476DE7

a72 inv-a72 >NUL
if not exist inv-a72.com goto end

echo.
echo INV-A72.COM     %CKSUM%
crc32 inv-a72.com
echo.

if "%1"=="notclean" goto end
del fix?.sed >NUL
del inv-a72.asm >NUL

:end
for %%z in (CKSUM JMPS) do set %%z=
if "%SED%"=="minised" set SED=
