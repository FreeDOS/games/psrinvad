@echo off

::#--- fix.sed begins ---
:: 1s/.*/ Jump+\n&/
:: /^;/b
:: /CODE_SEG/d
:: /^ *END /d
:: /ENDP/s/^[^ ]*//
:: / DB /y/"/'/
:: /SH[RL]/s/,1//
:: s/ Ptr//
:: s/40://
:: s/\[\([0-9]\)\]/+\1/
:: s/ DD / DW 0,/
:: /[CMI][MON][PVC] .*\[Bomb[YT]/s/\[/byte[/
:: /^[A-Z][^:]*:/s/:/ /
:: /ES:/s/\(.*\)\(ES:\)\(.*\)/ \2\n\1\3/
:: /LEA /s/LEA \(.*\),/MOV \1,OFFSET /
:: /^RemoveNewInt9/,/CLC$/{
:: /MOV /!b
:: /,W/b cs
:: /,[01]$/b cs
:: /StoreAX/!b
:: :cs
:: s/^/ CS:\n/
:: }
::#--- fix.sed ends ---

if not exist %0 %0.bat %1 %2
if "%1"=="-jmps" goto jmps
if not exist invaders.asm goto bye

if "%SED%"=="" set SED=minised
echo.
echo %%SED%% = '%SED%'
%SED% -n -e "/\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv-wolf.asm

call %0 -jmps inv-wolf
if errorlevel 1 goto end
if not exist inv-wolf.com goto end
call %0 -jmps inv-wolf

wasm.com inv-wolf >NUL

echo.
echo INV-WOLF.COM    FFF22EF9
crc32 inv-wolf.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del inv-wolf.asm >NUL

:end
if "%SED%"=="minised" set SED=
goto bye

:jmps
wasm.com %2|%SED% -n -e "/JMPS:/s/.*(\([0-9]*\)).*/\1s,JMP,\&S,/w jmps.sed"
ren %2.asm jmps.*
%SED% -f jmps.sed jmps.asm >%2.asm
del jmps.* >NUL

:bye
