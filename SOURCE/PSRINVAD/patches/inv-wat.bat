@echo off

::#--- fix.sed begins ---
:: 1i\
:: B equ byte ptr\
:: W equ word ptr\
:: include inv-wat.inc
:: /^;/b
:: s/ DD / DW 0,/
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/s_\1 equ \2/w inv-wat.inc
:: g
:: b
:: }
:: /CS:/b
:: /Ptr/b
:: s/LEA \(.*\),/MOV \1,OFFSET /
:: /,O/b
:: s/40:/DS:/
:: s/\[\(.*\)[+]\(BX\]\)/s_\1[\1][\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\(\[[0-9]\]\)/s_\1[\1]\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
::#--- fix.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised
if "%WATASM%"=="" set WATASM=wasm
echo %%SED%%    = '%SED%'
echo %%WATASM%% = '%WATASM%'
if not "%WASM%"=="" echo %%WASM%%   = '%WASM%'

%SED% -n -e "/fix\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv-wat.asm
if not exist inv-wat.inc goto end

%WATASM% -zq inv-wat
if not exist inv-wat.obj goto end
wlink op q format dos com file inv-wat
if not exist inv-wat.com warplink /c inv-wat >NUL
if not exist inv-wat.com goto end

echo.
echo INV-WAT.COM     9A476DE7
crc32 inv-wat.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del inv-wat.obj >NUL
del inv-wat.inc >NUL
del inv-wat.asm >NUL

:end
if "%WATASM%"=="wasm" set WATASM=
if "%SED%"=="minised" set SED=
