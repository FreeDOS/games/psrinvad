@echo off

::#--- fix1.sed begins ---
:: s/\([0-9][0-9a-fA-F]*\)h/0x\1/g
:: / DD /s// DW 0,/
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/s,s_\1\\[,[\2,/
:: s/,\[B,/,byte[,/
:: s/,\[W,/,word[,/
:: /,[bw]...\[,/w fix2.sed
:: g
:: s/^[A-Z][^ ]*/&:/
:: /"/s/ DB / .ASCIZ /
:: /ASCIZ/s/,0$//
:: b
:: }
:: /^;/b
:: /LEA /{
:: s//MOV /
:: s/,/,#/
:: b
:: }
:: s/\[0\]//
:: /CODE_SEG/d
:: /END/d
:: s/ *PROC .*/:/
:: s/40://
:: /,O[fF][^ ]* /{
:: s//,#/
:: b
:: }
:: /,[0-9]/s/,/,#/
:: s/Word Ptr //
:: s/\[\(.*\)[+]BX\]/s_\1[BX+\1]/
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: /RemoveNewInt9:/,/CLC/s/\[[^0]/CS:&/
:: /S:\[/s/\(.*\)\([CE]\)S:\(\[.*\)/\2SEG\n\1\3/
::#--- fix1.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised

echo.
echo %%SED%% = '%SED%'

%SED% -n -e "/\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix1.sed" %0
if not exist fix1.sed goto end
%SED% -f fix1.sed invaders.asm | %SED% -f fix2.sed >inv-as86.tmp
as86 inv-as86.tmp -b inv-as86.com -s NUL >inv.tmp
%SED% -n -e "s/^0*\([0-9]*\).* EB .*/\1s,JMP ,\&near ,/p" inv.tmp >fix3.sed
%SED% -f fix3.sed inv-as86.tmp >inv-as86.asm
del inv*.tmp >NUL

as86 inv-as86.asm -b inv-as86.com -s NUL
if not exist inv-as86.com goto end

echo.
echo INV-AS86.COM    FFF22EF9
crc32 inv-as86.com
echo.

if "%1"=="notclean" goto end
del fix?.sed >NUL
del inv-as86.asm >NUL

:end
if "%SED%"=="minised" set SED=
