@echo off
REM ... tested old NASM 0.97 (DOS, 1997), which lacks "-O" optimizations ...

::#--- opt.awk begins ---
:: /JMP/ && !/SHORT/ && ($3 ~ /E9([0-7][0-9A-F]00|[89A-F][0-9A-F]FF)/){
::  jmpfix[$1]=1
:: }
:: /CMP|ADD|SUB/ && ($3 ~ /00$/){
::  s=$0 ; sub(".*,","",s) ; if (s+0 <= 127) sbytefix[$1]=1
:: }
:: END{
::  while ((getline < "inv-nasm.asm") > 0){
::   lineno++
::   if (lineno in jmpfix) sub("JMP ","&SHORT ")
::   else if (($0 !~ /,byte/) && (lineno in sbytefix)) sub(",",",byte ")
::   print > "inv-nasm.new"
::  }
:: }
::#--- opt.awk ends ---

if not exist %0 %0.bat %1
if "%1"=="-inc" goto inc
if "%1"=="-asm" goto asm
if "%NASMEXE%"=="" set NASMEXE=nasm97
if "%AWK%"=="" set AWK=awk
:begin
set INV=inv-nasm
if exist %INV%.asm if exist %INV%.inc goto showenv
echo.
echo %INV%.ASM , %INV%.INC not found!
echo.
goto end
:showenv
echo.
echo %%AWK%%     = '%AWK%'
echo %%NASMEXE%% = '%NASMEXE%'
if not "%NASM%"=="" echo %%NASM%% = '%NASM%'
echo.
:build
call %0 -inc
call %0 -asm awk
call %0 -asm awk
call %0 -asm
if not exist %INV%.com goto end
:cksum
echo.
echo INV-NASM.COM    FFF22EF9
crc32 %INV%.com
echo.
if "%1"=="notclean" goto end
del %INV%.lst >NUL
del %INV%.asm >NUL
goto end
:inc
REM ... line numbers in *.LST must be accurate ...
ren %INV%.asm *.in~
set I1={if($0~/include/)
set P1=print};print}
%AWK% "%I1%{while((getline<\"%INV%.inc\")>0)%P1%" %INV%.in~ >%INV%.asm
for %%z in (I1 P1) do set %%z=
del %INV%.in? >NUL
goto bye
:asm
%NASMEXE% -f bin -o %INV%.com -l %INV%.lst %INV%.asm
if not exist %INV%.com goto bye
dir %INV%.com | %AWK% "/[cC][oO][mM]/"
if not "%2"=="awk" goto bye
:awk
set B1=[b]egins ---
set E1=[e]nds ---$
set L1=%INV%.lst
%AWK% "/\.awk %B1%/,/ %E1%/{sub(\"^:: *\",\"\");print}" %0 | %AWK% -f - %L1%
for %%z in (B1 E1 L1) do set %%z=
del %INV%.asm >NUL
ren %INV%.new *.asm >NUL
goto bye
:end
set INV=
if "%AWK%"=="awk" set AWK=
if "%NASMEXE%"=="nasm97" set NASMEXE=
:bye
