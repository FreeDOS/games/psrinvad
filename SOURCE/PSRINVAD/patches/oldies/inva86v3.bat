@echo off
REM ... tested only with old A86 3.22 (circa 1990) ...

::#--- fix.sed begins ---
:: /CODE_SEG/d
:: / END/d
:: s/ DD / DW 0,/
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/s_\1 equ \2/w 0inv.a86
:: g
:: }
:: s/\[0\]$//
:: /LEA /!{
:: /,O/!{
:: s/Word Ptr //
:: s/\[\(.*\)\([+]BX\]\)/s_\1[\1\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([0-9]\]\)/s_\1[\1+\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)/,s_\1[\1]/
:: s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: }
:: }
:: /^RemoveNewInt9 *PROC /,/CLC/{
:: s/40://
:: /Old/s/^/cs:/
:: /,[01]$/s/^/cs:/
:: /StoreAX/s/^/cs:/
:: }
:: /^; Sprite data/,${
:: w 3inv.a86
:: d
:: }
:: /^DisplayDigit/,${
:: w 2inv.a86
:: d
:: }
:: w 1inv.a86
::#--- fix.sed ends ---

if not exist %0 %0.bat %1
if "%SED%"=="" set SED=minised
if not exist invaders.asm goto end
echo %%SED%% = '%SED%'
echo.

%SED% -n -e "/\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end

%SED% -n -f fix.sed invaders.asm
for %%z in (0 1 2 3) do if not exist %%zinv.a86 goto cleanup
a86 +S ?inv.a86 inv-a86 >NUL
if not exist inv-a86.com goto cleanup
dir inv-a86.com | find /i "com"
echo.
echo INV-A86.COM     F9A3317C
crc32 inv-a86.com
echo.
:cleanup
if "%1"=="notclean" goto end
del ?inv.a86 >NUL
del fix.sed >NUL
:end
if "%SED%"=="minised" set SED=
