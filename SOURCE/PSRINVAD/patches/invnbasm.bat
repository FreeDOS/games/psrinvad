@echo off

::#--- fix1.sed begins ---
:: 1i\
:: .model tiny\
:: .code
:: s/ DD / DW 0,/
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/s,s_\1\\[,[\2,/
:: s/,\[B,/,byte [,/
:: s/,\[W,/,word [,/
:: /,[bw]... \[,/w fix2.sed
:: g
:: s/^[A-Z][^ ]*/&:/
:: b
:: }
:: /^;/b
:: /LEA /{
:: s//MOV /
:: s/,/,offset /
:: b
:: }
:: /CODE_SEG/d
:: /ENDP/d
:: /END /s//.&/
:: s/ *PROC .*/:/
:: s/40://
:: s/\[0\]//
:: /,O/b
:: s/Word Ptr //
:: s/\[\(.*\)[+]BX\]/s_\1[BX+\1]/
:: s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: /[A-Z][A-Z],/s/s_.*\[/[/
:: /,[A-Z][A-Z]/s/s_.*\[/[/
::#--- fix1.sed ends ---

if not exist %0 %0.bat %1 %2
if "%1"=="-jmps" goto jmps
if not exist invaders.asm goto bye

if "%SED%"=="" set SED=minised
if "%NBASMEXE%"=="" set NBASMEXE=nbasm32d

echo.
echo %%SED%%      = '%SED%'
echo %%NBASMEXE%% = '%NBASMEXE%'

%SED% -n -e "/\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix1.sed" %0
if not exist fix1.sed goto end
%SED% -f fix1.sed invaders.asm >nb.as~
%SED% -f fix2.sed -e "/RemoveNewInt9:/,/CLC/s/\[[^0]/CS:&/" nb.as~ >invnbasm.asm
del nb.as~ >NUL

call %0 -jmps invnbasm
if errorlevel 1 goto end
if not exist invnbasm.com goto end
call %0 -jmps invnbasm

%NBASMEXE% invnbasm >NUL
if not exist invnbasm.com goto end

echo.
echo INVNBASM.COM    188CF1B6
crc32 invnbasm.com
echo.

if "%1"=="notclean" goto end
del fix?.sed >NUL
del invnbasm.asm >NUL

:end
if "%NBASMEXE%"=="nbasm32d" set NBASMEXE=
if "%SED%"=="minised" set SED=
goto bye

:jmps
%NBASMEXE% %2|%SED% -n -e "/Diag/s/.*( *\([0-9]*\)).*/\1s,JMP,\&S,/w jmps.sed"
ren %2.asm jmps.*
%SED% -f jmps.sed jmps.asm >%2.asm
del jmps.* >NUL

:bye
