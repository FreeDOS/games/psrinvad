@echo off
if "%SED%"=="" goto end
if "%2"=="" goto end
if "%1"=="" goto end
if not exist %1.a86 goto end
for %%a in (com lst) do if not exist %2.%%a goto end
if not exist %1.inc goto build
%SED% -e "/include/r %1.inc" -e "/include/d" %1.a86 >new.a86
if not exist new.a86 goto end
del %1.a86 >NUL
ren new.a86 %1.*
del %1.inc >NUL
:build
%ASM86% +SL %1.a86 %2 >NUL
set E9=E9 [0-7][0-9A-F] 00
%SED% -n -e "s/^ *\([^ ][^ ]*\) [^ ][^ ]*  %E9%  *JMP .*/\1b z/w jmps" %2.lst
set E9=
echo b,:z| %SED% -e "s/,/\n/" >>jmps
%SED% -f jmps -e "s/JMP /&SHORT /" %1.a86 >newer.a86
del jmps >NUL
del %1.a86 >NUL
ren newer.a86 %1.*
%ASM86% +SL %1.a86 %2 >NUL
dir %2.com | %SED% -e "/[cC][oO][mM]/!d"
:end
