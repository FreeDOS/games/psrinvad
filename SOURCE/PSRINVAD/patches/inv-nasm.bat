@echo off

::#--- fix.sed begins ---
:: 1i\
:: %idefine offset\
:: %define LEA MOV\
:: %define B byte\
:: %define W word\
:: %include "inv-nasm.inc"
:: / DD /s// DW 0,/
:: / D[BW] /{
:: h
:: s/^\([A-Z][^ ]*\) *D\(.\) .*/%define s_\1 \2/w inv-nasm.inc
:: g
:: b
:: }
:: /^;/b
:: /LEA /b
:: /CODE_SEG/d
:: /END/d
:: / PROC /s/ *PROC .*/:/
:: /ES:\[/s//[ES:/
:: /40:/s///
:: /\[0\]/s///
:: /,O/b
:: /Word Ptr /s///
:: /[+]BX/s/\[\(.*\)[+]BX/s_\1&/
:: /\[[1-9]\]/s/\([A-Z][a-zA-Z][a-zA-Z][^ ]*\)\[\([1-9]\]\)/s_\1[\1+\2/
:: /,/s/\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\),/s_\1[\1],/
:: /,/s/,\([A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*\)/,s_\1[\1]/
:: /[ID][NE]C/s/\([ID][NE]C\) *\([A-Z][^ ][^ ][^ ]*\)/\1 s_\2[\2]/
:: /^RemoveNewInt9:/,/CLC$/s/\[\([^0]\)/[cs:\1/
::#--- fix.sed ends ---

if not exist invaders.asm goto end
if not exist %0 %0.bat %1

if "%SED%"=="" set SED=minised
if "%NASM%"=="" set NASM=nasm

echo.
echo %%SED%%     = '%SED%'
echo %%NASM%%    = '%NASM%'
if not "%NASMENV%"=="" echo %%NASMENV%% = '%NASMENV%'

%SED% -n -e "/fix\.sed begins ---/,/ ends ---$/s/^::[ ][ ]*//w fix.sed" %0
if not exist fix.sed goto end
%SED% -f fix.sed invaders.asm >inv-nasm.asm
if not exist inv-nasm.inc goto end

%NASM% -O3 inv-nasm.asm -o inv-nasm.com
if not exist inv-nasm.com goto end

echo.
echo INV-NASM.COM    FFF22EF9
crc32 inv-nasm.com
echo.

if "%1"=="notclean" goto end
del fix.sed >NUL
del inv-nasm.inc >NUL
del inv-nasm.asm >NUL

:end
if "%NASM%"=="nasm" set NASM=
if "%SED%"=="minised" set SED=
