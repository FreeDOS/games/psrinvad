#!/bin/awk -f

BEGIN {
  ASM="inv-oct.asm"; INC="inv-oct.inc"; LANG="LANG OCTASM,0.1"
  print LANG >INC
  print LANG,"use16 file_out \\inv-oct.com\n" \
    "define eq define\neq offset\n" \
    "eq segcs db 2Eh\neq seges db 26h\n" \
    "eq movtoal() db 0A0h dw #1\neq movalto() db 0A2h dw #1\n" \
    "eq movtoax() db 0A1h dw #1\neq movaxto() db 0A3h dw #1\n" \
    "\\" INC >ASM
}

!/^;/ && $2 ~ /^D[BWD]$/ {
  sub(" DD "," dw 0,"); gsub("\"","'")
  sub(" DB "," db "); sub(" DW "," dw ")
  print "eq s_" tolower($1),tolower(substr($2,2,1)) >INC
  s = substr($0,index($0,$2)); $1 = tolower($1); print "#" $1,s >ASM; next
}

/CODE_SEG|END/ { next }
!/^;/ && $1 ~ /:$/ { sub(":$","",$1); sub("^","#",$1) }
/LEA / { sub("LEA ","mov "); $0 = tolower($0); print >ASM; next }
/\[0\]|40:|Word Ptr / { sub("\\[0\\]|40:|Word Ptr ","") }
/^;|^ *$|,O[fF]/ { print tolower($0) >ASM; next }
/ES:/ { sub("ES:\\[","[es+") }
/ PROC / { sub(" *PROC .*",""); sub("^","#") }

/[+]BX/ {
  n=index($0,"+BX"); b=index($0,"[")
  $0 = substr($0,1,b-1) "s_" substr($0,b+1,n-b-1) substr($0,b)
}

/\[[1-9]\]/ {
  m=match($0,/[A-Z][a-zA-Z][a-zA-Z][^ ]*\[[1-9]\]/)
  b=index($0,"["); s=substr($0,m,b-m)
  $0 = substr($0,1,m-1) "s_" s "[" s "+" substr($0,b+1,1) "]" substr($0,b+3)
}

/INC |DEC / {
  op=2; if ($1 ~ /^#/) op++
  if ($op ~ /^[A-Z][^ ][^ ][^ ]*/) sub(".*","s_&[&]",$op)
}

/[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*,/ {
  m=match($0,/[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*,/); s=substr($0,m,RLENGTH-1)
  $0 = substr($0,1,m-1) "s_" s "[" s "]" substr($0,m+RLENGTH-1)
}

/,[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*/ {
  m=match($0,/,[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*/)
  s=substr($0,m+1); sub(" *;.*$","",s)
  $0 = substr($0,1,m) "s_" s "[" s "]" substr($0,m+RLENGTH)
}

/^#RemoveNewInt9/,/CLC/ { if ($0 !~ /\[0/) sub("\\[","[cs+") }

/MOV / && !/[+]BX/{
  sub(" *;.*","")

  if (($0 ~ /A[LX],.*\[[^ ][^ ][^ ][^ ]*\]/) && ($0 !~ /[+]DI\]/)){
    l=substr($0,index($0,",")-1,1)
    sub(",.*\\[",","); sub("\\]$","")
    sub("MOV *A" l ",","movtoa" l "("); sub("$",")")
  }
  else if ($0 ~ /\],A[LX]/) {
    l=substr($0,index($0,",")+2,1)
    sub("MOV .*\\[","mova" l "to("); sub("\\].*$",")")
  }

  if ($0 ~ /,0/) { sub(",00h.*",",0"); sub(",0 *;.*",",0") }

  if ($0 ~ /AX,/) sub("MOV *AX,0$","db 0B8h,0,0 ;&")
  else if ($0 ~ /BX,/) sub("MOV *BX,0$","db 0BBh,0,0 ;&")
  else if ($0 ~ /DI,/) sub("MOV *DI,0$","db 0BFh,0,0 ;&")

  if ($0 ~ /[(][ce]s[+]/) {
    $0 = "seg" substr($0,index($0,"(")+1,2) " " $0
    sub("[ce]s[+]","")
  }
}

{ $0 = tolower($0); print >ASM }
