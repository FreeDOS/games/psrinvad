#!/bin/awk -f

BEGIN {
  INC="inv-nasm.inc"; ASM="inv-nasm.asm"
  print "%idefine offset" >ASM ; print "%define LEA MOV" >ASM
  print "%define B byte" >ASM  ; print "%define W word" >ASM
  print "%include '" INC "'" >ASM
}

/CODE_SEG|END/ { next }
/\[0\]|40:|Ptr / { sub("\\[0\\]|40:|Word.Ptr ","") }
/^;|^ *$|LEA |,O[fF]/ { print >ASM; next }
/ PROC / { sub(" *PROC .*",":") }
/ES:/ { sub("ES:\\[","[ES:") }

/ DD / { sub(" DD "," DW 0,") }
$2 ~ /^D[BW]$/ {
  print "%define s_"$1,substr($2,2,1) >INC
  print >ASM; next
}

/[+]BX/ {
  n=index($0,"+BX"); b=index($0,"[")
  $0 = substr($0,1,b-1) "s_" substr($0,b+1,n-b-1) substr($0,b)
}

/\[[1-9]\]/ {
  m=match($0,/[A-Z][a-zA-Z][a-zA-Z][^ ]*\[[1-9]\]/)
  b=index($0,"["); s=substr($0,m,b-m)
  $0 = substr($0,1,m-1) "s_" s "[" s "+" substr($0,b+1,1) "]" substr($0,b+3)
}

/INC |DEC /{
  op=2; if ($1 ~ /:$/) op++
  if ($op ~ /^[A-Z][^ ][^ ][^ ]*/) sub(".*","s_&[&]",$op)
}

/[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*,/{
  match($0,/[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*,/)
  s=substr($0,RSTART,RLENGTH-1)
  $0 = substr($0,1,RSTART-1) "s_" s "[" s "]" substr($0,RSTART+RLENGTH-1)
}

/,[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*/{
  match($0,/,[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*/)
  s=substr($0,RSTART+1,RLENGTH-1)
  $0 = substr($0,1,RSTART) "s_" s "[" s "]" substr($0,RSTART+RLENGTH)
}

/^RemoveNewInt9:/,/CLC/ { if ($0 !~ /\[0/) sub("\\[","[cs:") }

{ print >ASM }
