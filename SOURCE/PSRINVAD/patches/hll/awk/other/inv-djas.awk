#!/bin/awk -f

# mawk -f asmdata.awk -f inv-djas.awk invaders.asm
# djasm inv-djas.asm inv-djas.com
# .COM = 9194 bytes, CRC32 = 0xFFF22EF9
END {
  ASM="inv-djas.asm"
  noshort="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\\`abcdefy"

  for (i=1; i <= NR; i++) {
    s=src[i]

    if (s !~ /["]/) s=tolower(s) #"
    else s=tolower(substr(s,1,match(s," D[BW] ")+3)) substr(s,RSTART+4)

    if ((s=="") || (s ~ /code_seg|endp|end +begin/)) continue
    sub("ds:|word ptr | ptr|\\[0\\]","",s); sub("lea ","mov ",s)
    sub(",offset ",",",s); sub(" *proc .*",":\n",s); sub(",09",",9",s)

    if (s ~ /^removenewint9:|clc$/) cseg = !cseg
    if (cseg && (s ~ /mov.*\[[^0]/)) sub("\\[","&cs:",s)

    gsub("[0-9][0-9a-f]*h","0x&0x",s); gsub("h0x","",s)
    sub("d[bw] |org",".&",s); if (s ~ /\.d[bw]/) sub("^[^ ]+","&:",s)
    if (s ~ /^[a-z0-9]+:/) {
      print substr(s,1,index(s,":")) >ASM
      s=substr(s,index(s,":")+1)
    }
    if (s ~ /[+]bx/) { sub("[+]bx","",s); sub("\\[","&bx+",s) }

    if ((s ~ /(byte|word)\[.*,[0-9]/) || (s ~ /inc |dec /))
      sub(" *(byte|word)\\[",substr(s,index(s,"[")-4,1)"[",s)
    else if (s ~ /,[abcd][hlx]| [abcd][hlx],|,[es][si]| si,/)
      sub(" *(byte|word)","",s)

    if ((s ~ /jmp /) && (index(noshort,sprintf("%c",++jumpnum+32))))
      sub("jmp ","jmpl ",s)

    if (s ~ /test/)
      if (s ~ /al,/) sub("test.*al,",".db 0xA8,",s)
      else if (s ~ / *ax,/) sub("test *ax,",".db 0xA9\n.dw ",s)

    if (match(s,/,0x0*[0-9]$/)) sub(",0x0*.$","," substr(s,length(s),1),s)

    if (match(s,/cmpw\[.*\]/) && (s ~ /,[0-9]$|,[0-9][0-9]$/))
      sub(".*,",".db 0x83,0x3e\n.dw " substr(s,RSTART+5,RLENGTH-6) \
          "\n.db " substr(s,index(s,",")+1) ";&",s)
    else if (match(s,/subw\[.*\]/) && (s ~ /,[0-9]$/))
      sub(".*,",".db 0x83,0x2e\n.dw " substr(s,RSTART+5,RLENGTH-6) \
          "\n.db " substr(s,index(s,",")+1) ";&",s)
    else if (match(s,/addw\[.*\]/) && (s ~ /,[0-9][0-9]*$/))
      sub(".*,",".db 0x83,6\n.dw " substr(s,RSTART+5,RLENGTH-6) \
          "\n.db " substr(s,index(s,",")+1) ";&",s)

    print s >ASM
  }
}
# EOF
