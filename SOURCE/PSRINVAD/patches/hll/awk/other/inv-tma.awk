#!/bin/awk -f

# mawk -f asmdata.awk -f inv-tma.awk invaders.asm
# tma inv-tma.asm
# .COM = 9194 bytes, CRC32 = 0x6C0F5986
END {
  for (i=1; i <= NR; i++) {
    s=src[i]
    if ((s ~ /CODE_SEG|END/) || (s=="")) continue
    sub("ds:|\\[0\\]","",s); sub("Exit","&s",s);
    sub(" *PROC .*",":",s); sub("LEA ","MOV ",s)
    if (s ~ /^RemoveNewInt9:|CLC/) cseg = !cseg
    if (cseg && (s ~ /\[[^0]/)) sub("\\[","[cs:",s)
    print s >"inv-tma.asm"
  }
}
# EOF
