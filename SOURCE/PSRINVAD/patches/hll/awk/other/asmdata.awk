#!/bin/awk -f

# N.B. Runs out of memory on 16-bit OW-built BWK's AWK!
# (Try 16-bit MAWK or a 32-bit pmode build of BWK's AWK or DJGPP's GAWK.)

{
  if (jwasm)
    print "ifdef __JWASM__\noption noscoped,nokeyword:<lea>\n" \
          "lea macro a,b\nmov a,offset b\nendm\nendif"

  do {
    gsub("\t"," "); sub(" *","")
    sub(" *DB *"," DB "); sub(" *DW *"," DW ")

    sub(" *;.*","")
    sub("40:","ds:"); sub(" DD "," DW 0,"); sub("Word Ptr ","")

    src[NR] = $0

    if (($0 !~ /^;/) && (NF > 1)) {
      if      ($2 ~ /^DB$/) byte[$1]=1
      else if ($2 ~ /^DW$/) word[$1]=1
    }
  } while (getline > 0)

  for (i=1; i <= NR; i++) {

    if ((src[i] ~ /,|INC |DEC /) && (src[i] !~ /,O[fF]| D[BW] |LEA /)) {
      add=""; it=""
      if (src[i] ~ /[+]BX/) {
        sub("\\[","",src[i]); sub("[+]",",[",src[i])
      }
      if (src[i] ~ "\\[[0-9]\\]") {
        add=src[i]; sub(".*\\[","\\[",add); sub("\\].*","\\]",add)
        sub(add,"",src[i]); gsub("[^0-9]","",add); add="+" add
      }
      max=split(src[i],line,",")
      for (j=1; j <= max; j++) {
        cmax=split(line[j],chunk," ")
        for (k=1; k <= cmax; k++) {
          duh=" ptr[" chunk[k] add "]"
          if (chunk[k] in byte) sub(chunk[k],"byte" duh,line[j])
          else if (chunk[k] in word) sub(chunk[k],"word" duh,line[j])
        }
        it=it line[j]; if (j != max) it=it ","
      }
      sub("\\],\\[","+",it)
      src[i] = it
    }
  }

  if (show) for (i=1; i <= NR; i++) if (length(src[i]) > 0) print src[i]
}
# EOF
