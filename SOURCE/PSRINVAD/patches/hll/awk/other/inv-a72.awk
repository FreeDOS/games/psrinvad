#!/bin/awk -f

# mawk -f asmdata.awk -f inv-a72.awk invaders.asm
# a72 inv-a72.asm
# .COM = 9194 bytes, CRC32 = 0x9A476DE7
END {
  OUT="inv-a72.asm"
  noshort="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\\`abcdefy"
  for (i=1; i <= NR; i++) {
    s=src[i]
    if ((s ~ /CODE_SEG|END/) || (s=="")) continue
    sub("ds:| ptr|\\[0\\]","",s)
    sub(" *PROC .*",":",s); sub("LEA ","MOV ",s)
    if (s ~ /^RemoveNewInt9:|CLC/) cseg = !cseg
    if (cseg && (s ~ /\[[^0]/)) sub("\\[","[CS:",s)
    if (s ~ /[CE]S:/) {
      n=index(s,":")-2
      print substr(s,n,3) "\n" >OUT
      s=substr(s,1,n-1) substr(s,n+3)
    }
    if ((s ~ /JMP/) && (!index(noshort,sprintf("%c",++jumpnum+32))))
      sub("JMP ","&SHORT ",s)
    print s >OUT
  }
}
# EOF
