#!/bin/awk -f

# mawk -f asmdata.awk -f inv-tiny.awk invaders.asm
# tinyasm -o inv-tiny.com inv-tiny.asm  (NASM also works)
# .COM = 9194 bytes, CRC32 = 0xFFF22EF9
END {
  for (i=1; i <= NR; i++) {
    s=src[i]
    if ((s ~ /CODE_SEG|END/) || (s=="")) continue
    sub("ds:| ptr|\\[0\\]","",s); sub(" *PROC .*",":",s)
    gsub("[0-9][0-9a-fA-F]*h","0x&0x",s); gsub("h0x","",s)
    if (s ~ /MOV[^;]*[, ]A[LX]/) sub("[bw]...\\[","[",s)
    sub("LEA ","MOV ",s); sub(",O[fF][^ ]*",",",s)
    if (s ~ /ES:/) { sub("ES:","",s); sub("^","ES ",s) }
    if (s ~ /[+]BX/) { sub("[+]BX","",s); sub("\\[","&BX+",s) }
    if (s ~ / D[BW] /) sub("^[A-Z][^ ]*","&:",s)
    if (s ~ /^RemoveNewInt9:|CLC/) cseg = !cseg
    if (cseg && (s ~ /\[[^0]/)) sub("^","CS ",s)
    print s >"inv-tiny.asm"
  }
}
# EOF
