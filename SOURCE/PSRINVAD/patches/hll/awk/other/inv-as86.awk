#!/bin/awk -f

# mawk -f asmdata.awk -f inv-as86.awk invaders.asm
# as86 inv-as86.asm -b inv-as86.com -s NUL
# .COM = 9194 bytes, CRC32 = 0xFFF22EF9
END {
  noshort="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\\`abcdefy"
  for (i=1; i <= NR; i++) {
    s=src[i]
    if ((s ~ /CODE_SEG|END/) || (s=="")) continue
    sub("ds:|ptr|Word Ptr |\\[0\\]","",s); sub(" *PROC .*",":",s)
    gsub("[0-9][0-9a-fA-F]*h","0x&0x",s); gsub("h0x","",s)
    sub(",O[fF][^ ]* ",",#",s)
    if (s ~ /LEA /) { sub("LEA ","MOV ",s); sub(",",",#",s) }
    if ((s ~ /,[0-9]/) && (s !~ / D[BW] /)) sub(",",",#",s)
    if (s ~ /[+]BX/) { sub("[+]BX","",s); sub("\\[","&BX+",s) }
    if (s ~ / D[BW] /) sub("^[A-Z][^ ]*","&:",s)
    if (s ~ / DB +[^0].*,0$/) {sub(" DB "," .ASCIZ ",s); sub(",0$","",s) }
    if (s ~ /ES:/) { sub("ES:","",s); sub("^","ESEG\n",s) }
    if (s ~ /^RemoveNewInt9:|CLC/) cseg = !cseg
    if (cseg && (s ~ /\[[^0]/)) sub("^","CSEG\n",s)
    if ((s ~ /JMP/) && (index(noshort,sprintf("%c",++jumpnum+32))))
      sub("JMP ","&near ",s)
    print s >"inv-as86.asm"
  }
}
# EOF
