#!/bin/awk -f

# mawk -f asmdata.awk -f invnbasm.awk invaders.asm
# nbasm32d invnbasm.asm
# .COM = 9194 bytes, CRC32 = 0x188CF1B6
END {
  OUT="invnbasm.asm"
  noshort="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\\`abcdefy"
  print ".model tiny\n.code" >OUT
  for (i=1; i <= NR; i++) {
    s=src[i]
    if ((s ~ /CODE_SEG|ENDP/) || (s=="")) continue
    sub("ds:|ptr|\\[0\\]","",s)
    sub(" *PROC .*",":",s)
    if (s ~ / D[BW] /) sub("^[A-Z][^ ]*","&:",s)
    if (s ~ /LEA/) { sub("LEA ","MOV ",s); sub(",",",offset ",s) }
    if (s ~ /END /) sub("END ",".&",s)
    if (s ~ /[A-Z][A-Z],/) sub("[bw]... \\[","[",s)
    if (s ~ /,[A-Z][A-Z]/) sub("[bw]... \\[","[",s)
    if (s !~ /^[A-Z][^ ]*:/) sub("^"," ",s)
    if (s ~ /^RemoveNewInt9:|CLC/) cseg = !cseg
    if (cseg && (s ~ /\[[^0]/)) sub("\\[","CS:&",s)
    if ((s ~ /JMP/) && (!index(noshort,sprintf("%c",++jumpnum+32))))
      sub("JMP ","&SHORT ",s)
    print s >OUT
  }
}
# EOF
