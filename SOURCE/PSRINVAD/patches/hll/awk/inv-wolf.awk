#!/bin/awk -f

BEGIN {
  noshort="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\\`abcdefy"
}

/CODE_SEG|END / { next }
/\[0\]| Ptr|40:/ { sub("\\[0\\]| Ptr|40:","") }
/^[A-Z][^ ]*:/ { sub(":"," ") }
/ ENDP/ { sub("^[^ ]*","") }
/ DB / { gsub("\"","'") }
/SH[RL]/ { sub(",1","") }
/ DD / { sub(" DD "," DW 0,") }
/(CMP|MOV|INC) .*\[Bomb[YT]/ { sub("\\[","byte[") }
/ES:\[/ { sub("ES:\\[","["); $0 = " ES:\n" $0 }
/LEA / { sub("LEA ","MOV "); sub(",",",OFFSET ") }
/\[[1-9]\]/ { sub("\\[[1-9]\\]","_&_"); sub("_\\[","+"); sub("\\]_","") }

/^RemoveNewInt9/,/CLC$/{
  if ($0 ~ /MOV .*(,W|,[01]$|StoreAX)/) sub("^"," CS:\n")
}

!/^;/ && /JMP / {
  if (!index(noshort,sprintf("%c",++jumpnum+32))) sub("JMP ","JMPS ")
}

{ print >"inv-wolf.asm" }
