#!/bin/awk -f

BEGIN {
  while ((getline < "inv-nasm.inc") > 0)
    if ($3 == "B") bytenames[substr($2,3,length($2)-2)] = 1
}

(length > 0) && (substr($0,1,1) != "%") {
  if (($0 ~ " D[BW] ") && ($0 ~ /^[A-Z]/)) sub("^[^ ]*","&:")
  gsub("[0-9][0-9a-fA-F]*h","0x&0x"); gsub("h0x","")

  if (index($0,"+BX") != 0) {
    sub("\\[","&BX+"); sub("[+]BX","")
  }

  if (match($0,/\[[cCdDeEsS][sS]:/))
    $0 = substr($0,RSTART+1,2) " " substr($0,1,RSTART) substr($0,RSTART+4)

  sub("LEA ","MOV "); sub(",O[fF][^ ]* ",",")

  if ((b=index($0,"s_")) != 0) {
    b2=index($0,"[")
    if (substr($0,b+2,b2-b-2) in bytenames) size="byte"; else size="word"
    $0 = substr($0,1,b-1) size substr($0,b2)
  }

  if ($0 ~ /,A[LX]| A[LX],/) sub("byte|word","")

  print >"inv-tiny.asm"
}
