#!/bin/awk -f

BEGIN {
  ASM="inv.a86"; INC="inv.inc"
  noshort="#$%&,./123456789=>DGJMOPQRSTUVWXYZ][_^\\`abcdefy"
}

!/^;/ && ($2 ~ /D[BW]/) { print "EXTRN",$1 ":" substr($2,2,1) > INC }

/BEGIN:/ { sub("$","include " INC) }
/\[0\]$/ { sub("\\[0\\]$","") }

/RemoveNewInt9 *PROC /,/CLC/ {
  if ($0 ~ /,Word|,[01]$|StoreAX/) sub("^","cs:")
  sub("40:","")
}

# TIDY.SED output also works
!/^;/ && /JMP / {
  if (!index(noshort,sprintf("%c",++jumpnum+32))) sub("JMP ","&SHORT ")
}

{ print > ASM }
