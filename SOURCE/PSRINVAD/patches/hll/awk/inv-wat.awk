#!/bin/awk -f

BEGIN {
  ASM="inv-wat.asm"; INC="inv-wat.inc"
  print "B equ byte ptr\nW equ word ptr\ninclude",INC >ASM
}

!/^;/ && $2 ~ /^D[BWD]$/ {
  sub(" DD "," DW 0,")
  print "s_" $1,"equ",substr($2,2,1) >INC
  print >ASM; next
}

/CODE_SEG|,O[fF]/ { print >ASM; next }
/LEA / { sub("LEA ","MOV "); sub(",",",OFFSET "); print >ASM; next }

/\[0\]|Word Ptr/ { sub("\\[0\\]|Word Ptr","") }
/40:/ { sub("40:","DS:") }

/[+]BX/ {
  c=index($0,"["); s=substr($0,c+1,index($0,"]")-c-length("+BX]"))
  sub("\\[.*\\]","s_" s "[" s "+BX]")
}

/\[[1-9]\]/ {
  m=match($0,/[A-Z][a-zA-Z][a-zA-Z][^ ]*\[[1-9]\]/)
  b=index($0,"["); s=substr($0,m,b-m)
  $0 = substr($0,1,m-1) "s_" s "[" s "+" substr($0,b+1,1) "]" substr($0,b+3)
}

/INC |DEC /{
  op=2; if ($1 ~ /:$/) op++
  if ($op ~ /^[A-Z][^ ][^ ][^ ]*/) sub(".*","s_&[&]",$op)
}

/[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*,/{
  m=match($0,/[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*,/); s=substr($0,m,RLENGTH-1)
  $0 = substr($0,1,m-1) "s_" s "[" s "]" substr($0,m+RLENGTH-1)
}

/,[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*/{
  m=match($0,/,[A-Z][a-zA-Z][a-zA-Z][a-zA-Z0-9]*/)
  s=substr($0,m+1); sub(" *;.*$","",s)
  $0 = substr($0,1,m) "s_" s "[" s "]" substr($0,m+RLENGTH)
}

{ print >ASM }
