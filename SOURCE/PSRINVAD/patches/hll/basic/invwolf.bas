' $lang: "fblite" (smaller .EXE than "qb")
' public domain, nenies proprajho, free for any use

DEFINT A-Z : CONST dquote=&H22
DIM SHARED l AS STRING,oldasm,newasm,override

DECLARE SUB Adjust()
DECLARE SUB Delstr(bad AS STRING)
DECLARE SUB Change(older AS STRING,newer AS STRING)
DECLARE SUB InsertSeg()
DECLARE SUB FixLabels()
DECLARE SUB FixJumps()

override=FALSE : oldasm=FREEFILE
IF ENVIRON$("INVADERS") <> "" THEN _
  OPEN ENVIRON$("INVADERS") FOR INPUT AS #oldasm _
ELSE _
  OPEN "INVADERS.ASM" FOR INPUT AS #oldasm

newasm = FREEFILE : OPEN "inv-wolf.asm" FOR OUTPUT AS #newasm

DO UNTIL EOF(oldasm)
  LINE INPUT #oldasm,l
  Adjust
LOOP
CLOSE #newasm,#oldasm
SYSTEM ' exit

SUB Adjust
  IF LEFT$(l,1)=";" OR LEN(l)=0 THEN GOTO writeline
  IF INSTR(l,"CODE_SEG") OR INSTR(l,"END ") THEN EXIT SUB

  Change " DD "," DW 0,"
  IF INSTR(l," DB ") THEN
    DO WHILE INSTR(l,CHR$(dquote)) : Change CHR$(dquote),"'" : LOOP
  END IF

  p=INSTR(l,"[")
  IF p <> 0 THEN
    IF MID$(l,p+2,1)="]" AND INSTR("0123456789",MID$(l,p+1,1)) THEN _
       l=MID$(l,1,p-1) + "+" + MID$(l,p+1,1) + MID$(l,p+3)
  END IF

  IF INSTR(l,"SHL ") OR INSTR(l,"SHR ") THEN Delstr ",1"

  IF INSTR(l,"LEA ") THEN
    Change ",",",OFFSET "
    Change "LEA ","MOV "
  END IF

  FixLabels

  IF INSTR(l,"CMP ") OR INSTR(l,"MOV ") OR INSTR(l,"INC ") THEN _
    IF INSTR(l,"BombT") OR INSTR(l,"BombY") THEN _
      Change "[","byte["

  IF INSTR(l,"JMP ") THEN FixJumps

  Delstr "40:" : Delstr "Ptr "

  IF INSTR(l,"ES:[") THEN
    Delstr "ES:" : ? #newasm," ES:"
  END IF

  InsertSeg

  writeline: ? #newasm,l
END SUB

SUB Delstr(bad AS STRING)
  del=INSTR(l,bad) : IF del <> 0 THEN l=MID$(l,1,del-1)+MID$(l,del+LEN(bad))
END SUB

SUB Change(older AS STRING,newer AS STRING)
  s=INSTR(l,older) : IF s <> 0 THEN l=MID$(l,1,s-1)+newer+MID$(l,s+LEN(older))
END SUB

SUB FixLabels
  IF LEFT$(l,1) >= "A" AND LEFT$(l,1) <= "Z" THEN
    p=INSTR(l,":") : IF p <> 0 THEN MID$(l,p,1)=" "
  END IF
  p=INSTR(l," ENDP") : IF p <> 0 THEN l=MID$(l,p)
END SUB

SUB InsertSeg
  IF INSTR(l,"RemoveNewInt9") THEN IF INSTR(l," PROC") THEN override=NOT FALSE
  IF override AND INSTR(l,"MOV ") THEN _
    IF INSTR(l,",W") OR INSTR(l,"StoreAX") OR _
      RIGHT$(l,2)=",0" OR RIGHT$(l,2)=",1" THEN _
        ? #newasm," CS:"
  IF INSTR(l,"CLC") THEN override=FALSE
END SUB

SUB FixJumps
  CONST noshort="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\`abcdefy"
  STATIC jumpnum
  jumpnum = jumpnum + 1
  IF INSTR(noshort,CHR$(jumpnum+ASC(" ")))=0 THEN _
    MID$(l,INSTR(l,"JMP ")+3,1)="S"
END SUB

' <EOF>
