' $lang: "fblite" (smaller .EXE than "qb")
' public domain, nenies proprajho, free for any use

DEFINT A-Z : CONST nasminc="inv-nasm.inc",upper$="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
DIM SHARED l AS STRING,oldasm,incfile,newasm

DECLARE SUB Adjust()
DECLARE SUB Delstr(bad AS STRING)
DECLARE SUB Change(older AS STRING,newer AS STRING)
DECLARE SUB InsertSeg()
DECLARE SUB IncDec()
DECLARE SUB FixProc()
DECLARE SUB FixOp1()
DECLARE SUB FixOp2()
DECLARE FUNCTION BrakDig%(n AS INTEGER)

oldasm=FREEFILE
IF ENVIRON$("INVADERS") <> "" THEN _
  OPEN ENVIRON$("INVADERS") FOR INPUT AS #oldasm _
ELSE _
  OPEN "INVADERS.ASM" FOR INPUT AS #oldasm

incfile = FREEFILE : OPEN nasminc FOR OUTPUT AS #incfile
newasm  = FREEFILE : OPEN "inv-nasm.asm" FOR OUTPUT AS #newasm

? #newasm,"%idefine offset" : ? #newasm,"%define LEA MOV"
? #newasm,"%define B byte"  : ? #newasm,"%define W word"
? #newasm,"%include '" ; nasminc ; "'"

DO UNTIL EOF(oldasm)
  LINE INPUT #oldasm,l
  Adjust
LOOP
CLOSE #newasm,#incfile,#oldasm
SYSTEM ' exit

SUB Adjust
  IF LEFT$(l,1)=";" OR INSTR(l,"LEA ") THEN GOTO writeline
  Change " DD "," DW 0,"

  IF INSTR(l," DB ") OR INSTR(l," DW ") THEN
    p = INSTR(l," ") : p2=p
    DO WHILE MID$(l,p2,1)=" " : p2=p2+1 : LOOP
    IF p <> 1 THEN _
      ? #incfile,"%define s_" ; MID$(l,1,p-1) ; " " ; MID$(l,p2+1,1)
    GOTO writeline
  END IF

  IF INSTR(l,"CODE_SEG") OR INSTR(l,"END") THEN EXIT SUB

  Delstr "[0]"

  IF INSTR(l,",O")=0 THEN
    Change "ES:[","[ES:" : Delstr "40:" : Delstr "Word Ptr " : FixProc

    p=INSTR(l,"],")
    IF p >= 4 THEN
      IF ((MID$(l,p-2,1)="[") AND (INSTR("0123456789",MID$(l,p-1,1)))) THEN _
        l = MID$(l,1,p-3) + "_" + MID$(l,p-1,1) + MID$(l,p+1)
    END IF

    IF INSTR(l,"+BX") THEN
      Delstr "[" : Delstr "]"
    END IF

    FixOp1
    FixOp2

    p=INSTR(l,"[") : IF BrakDig(p) THEN l = MID$(l,1,p-3) + MID$(l,p)
    p=INSTR(l,"]") : IF BrakDig(p) THEN MID$(l,p-2,1) = "+"

    IncDec
    Change "+BX[","[" : Change "[]","+" : Change "][","+" : Change "+[","+"

    InsertSeg
  END IF

  writeline: ? #newasm,l
END SUB

SUB Delstr(bad AS STRING)
  del=INSTR(l,bad) : IF del <> 0 THEN l=MID$(l,1,del-1)+MID$(l,del+LEN(bad))
END SUB

SUB Change(older AS STRING,newer AS STRING)
  s=INSTR(l,older) : IF s <> 0 THEN l=MID$(l,1,s-1)+newer+MID$(l,s+LEN(older))
END SUB

SUB FixProc
  p=INSTR(l," PROC ")
  IF p <> 0 THEN
    DO UNTIL MID$(l,p,1) <> " " : p=p-1 : LOOP
    l = MID$(l,1,p) + ":"
  END IF
END SUB

SUB FixOp1
  s$=LCASE$(upper$) + upper$ + "0123456789" + "+_"
  p2=INSTR(l,",")
  IF p2 <> 0 THEN
    IF MID$(l,p2-1,1) <> "]" THEN
      p2=p2-1 : p=p2
      DO UNTIL MID$(l,p,1)=" " OR INSTR(s$,MID$(l,p,1))=0 : p=p-1 : LOOP
      p=p+1
      IF ((p2-p+1) > LEN("DX")) AND MID$(l,p,1) <> "[" AND _
           INSTR(upper$,MID$(l,p,1)) THEN
             l = MID$(l,1,p-1) + "s_" + MID$(l,p,p2-p+1) + "[" + _
                 MID$(l,p,p2-p+1) + "]" + MID$(l,p2+1)
      END IF
    END IF
  END IF
END SUB

SUB FixOp2
  s$=LCASE$(upper$) + upper$ + "0123456789" + "+_"
  p=INSTR(l,",") : p2=INSTR(l,";")
  IF p <> 0 THEN
    IF ((p2=0) OR (p2 > p)) AND (MID$(l,p,1) <> "[") THEN
      p=p+1 : p2=p
      DO WHILE (p2 < LEN(l)) AND (MID$(l,p2,1) <> " ") AND _
        (INSTR(s$,MID$(l,p2,1)))
           p2=p2+1
      LOOP
      IF MID$(l,p2,1)=" " OR MID$(l,p2,1)="[" THEN p2=p2-1
      IF (p2 > p) AND (p2-p+1 > LEN("DX")) AND (INSTR(upper$,MID$(l,p,1))) THEN
        l = MID$(l,1,p-1) + "s_" + MID$(l,p,p2-p+1) + "[" + _
            MID$(l,p,p2-p+1) + "]" + MID$(l,p2+1)
      END IF
    END IF
  END IF
END SUB

SUB InsertSeg
  STATIC override
  IF INSTR(l,"RemoveNewInt9:") THEN override=NOT FALSE
  IF override AND INSTR(l,"[0")=0 THEN Change "[","[cs:"
  IF INSTR(l,"CLC") THEN override=FALSE
END SUB

SUB IncDec
  s$=LCASE$(upper$) + upper$ + "0123456789" + "+"
  p=INSTR(l,"INC ") : IF p=0 THEN p=INSTR(l,"DEC ")
  IF p <> 0 THEN
    p=p+LEN("??C ")
    DO WHILE MID$(l,p,1)=" " : p=p+1 : LOOP
    IF MID$(l,p,1) <> "[" THEN
      p2=p
      DO UNTIL (p2=LEN(l)) OR (MID$(l,p2,1)=" ") OR INSTR(s$,MID$(l,p2,1))=0
          p2=p2+1
      LOOP
      IF MID$(l,p2,1)=" " OR MID$(l,p2,1)="[" THEN p2=p2-1
      IF ((p2-p+1) > LEN("DX")) THEN
        l = MID$(l,1,p2) + "]" + MID$(l,p2+1)
        l = MID$(l,1,p-1) + "s_" + MID$(l,p,p2-p+1) + "[" + MID$(l,p)
      END IF
    END IF
  END IF
END SUB

FUNCTION BrakDig%(n AS INTEGER)
  BrakDig = FALSE
  IF n <> 0 THEN
    IF (MID$(l,n-2,1)="_") AND (INSTR("0123456789",MID$(l,n-1,1))) THEN _
      BrakDig = NOT FALSE
  END IF
END FUNCTION

' <EOF>
