' $lang: "fblite" (smaller .EXE than "qb")
' public domain, nenies proprajho, free for any use

DEFINT A-Z : CONST incname="inv.inc"
DIM SHARED l AS STRING,oldasm,incasm,newasm,override

DECLARE SUB Adjust()
DECLARE SUB InsertSeg()
DECLARE SUB FixJumps()

override=FALSE : oldasm=FREEFILE
IF ENVIRON$("INVADERS") <> "" THEN _
  OPEN ENVIRON$("INVADERS") FOR INPUT AS #oldasm _
ELSE _
  OPEN "INVADERS.ASM" FOR INPUT AS #oldasm

incasm=FREEFILE : OPEN incname FOR OUTPUT AS #incasm
newasm=FREEFILE : OPEN "inv.a86" FOR OUTPUT AS #newasm
DO UNTIL EOF(oldasm)
  LINE INPUT #oldasm,l
  Adjust
LOOP
CLOSE #newasm,#incasm,#oldasm
SYSTEM ' exit

SUB Adjust
  IF LEFT$(l,1)=";" OR LEN(l)=0 OR INSTR(l,"CODE_SEG") _
     OR INSTR(l,"END") THEN _
        goto writeline

  IF LEFT$(l,1) >= "A" AND LEFT$(l,1) <= "Z" THEN _
     IF INSTR(l," DB ") OR INSTR(l," DW ") THEN _
        ? #incasm,"EXTRN " ; MID$(l,1,INSTR(l," ")-1) ; ":" ; _
          MID$(l,INSTR(l," D")+2,1)

  IF INSTR(l,"BEGIN:") THEN
    l = l + "include " + incname
  ELSEIF INSTR(l,"[0]")=LEN(l)-2 THEN
    l=MID$(l,1,LEN(l)-3)
  ELSEIF INSTR(l,"40:") THEN
    MID$(l,INSTR(l,"40:"),3)="DS:"
  END IF

  IF INSTR(l,"JMP ") THEN FixJumps

  IF INSTR(l,"RemoveNewInt9") THEN IF INSTR(l," PROC ") THEN override=NOT FALSE
  IF override AND INSTR(l,"MOV ") THEN InsertSeg
  IF INSTR(l,"CLC") THEN override=FALSE

writeline: ? #newasm,l
END SUB

SUB FixJumps
  CONST noshort="#$%&,./123456789=>DGJMOPQRSTUVWXYZ][_^\`abcdefy"
  STATIC jumpnum
  jumpnum = jumpnum + 1
  IF INSTR(noshort,CHR$(jumpnum+ASC(" ")))=0 THEN _
     l = MID$(l,1,INSTR(l,"JMP ")+3) + "SHORT " + MID$(l,INSTR(l,"JMP ")+4)
END SUB

SUB InsertSeg
  IF INSTR(l,",W") OR INSTR(l,"StoreAX") _
     OR RIGHT$(l,2)=",0" OR RIGHT$(l,2)=",1" THEN _
        l = "CS" + l
END SUB

' <EOF>
