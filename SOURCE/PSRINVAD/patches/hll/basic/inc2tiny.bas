' $lang: "fblite" (smaller .EXE than "qb")
' public domain, nenies proprajho, free for any use

DEFINT A-Z : CONST maxbytenames=60, maxbytenamelen=19

DECLARE SUB Adjust()
DECLARE SUB getbytenames()
DECLARE SUB fixhexnums()
DECLARE FUNCTION isbytename%(n AS STRING)
DECLARE FUNCTION foundhexnum%()

DIM SHARED l AS STRING,bytenames(1 TO maxbytenames) AS STRING * maxbytenamelen
DIM SHARED hexbegin,hexend
DIM oldasm,newasm

getbytenames
oldasm=FREEFILE : OPEN "inv-nasm.asm" FOR INPUT AS #oldasm
newasm=FREEFILE : OPEN "inv-tiny.asm" FOR OUTPUT AS #newasm
DO UNTIL EOF(oldasm)
  LINE INPUT #oldasm,l
  IF (LEN(l) > 0) AND (INSTR(";%",MID$(l,1,1))=0) THEN
    Adjust
    ? #newasm,l
  END IF
LOOP
CLOSE #oldasm,#newasm
SYSTEM 'exit

SUB getbytenames
  DIM bytenum,incfile

  bytenum=1
  incfile=FREEFILE : OPEN "inv-nasm.inc" FOR INPUT AS #incfile
  DO UNTIL EOF(incfile)
    LINE INPUT #incfile,l
    IF MID$(l,LEN(l),1)="B" THEN
      l=MID$(l,INSTR(l,"s_")+2)
      bytenames(bytenum)=MID$(l,1,LEN(l)-2)
      bytenum=bytenum + 1
    END IF
  LOOP
  CLOSE #incfile
END SUB

FUNCTION isbytename%(n AS STRING)
  isbytename=FALSE
  FOR i = 1 TO maxbytenames
    IF RTRIM$(bytenames(i))=n THEN
      isbytename=NOT FALSE
      EXIT FUNCTION
    END IF
  NEXT i
END FUNCTION

SUB Adjust
  IF INSTR(l," DB ") OR INSTR(l," DW ") THEN _
    IF MID$(l,1,1) >= "A" AND MID$(l,1,1) <= "Z" THEN _
      MID$(l,INSTR(l," "),1)=":"

  p=INSTR(l,"+BX")
  IF p <> 0 THEN
    l=MID$(l,1,p-1) + MID$(l,p+3)
    b=INSTR(l,"[") : l=MID$(l,1,b) + "BX+" + MID$(l,b+1)
  END IF

  p=INSTR(l,"[")
  IF p <> 0 AND UCASE$(MID$(l,p+2,2))="S:" THEN
    myseg$=MID$(l,p+1,2)
    l=myseg$ + MID$(l,1,p) + MID$(l,p+4)
  END IF

  p=INSTR(l,"LEA ") : IF p <> 0 THEN MID$(l,p,3)="MOV"

  p=INSTR(l,",O")
  IF (p <> 0) AND UCASE$(MID$(l,p+1,2))="OF" THEN _
    IF UCASE$(MID$(l,p+1,6))="OFFSET" THEN l=MID$(l,1,p) + MID$(l,p+8)

  p=INSTR(l,"s_")
  IF p <> 0 THEN
    IF isbytename(MID$(l,p+2,INSTR(l,"[")-(p+2))) THEN size$="byte" _
    ELSE size$="word"
    l=MID$(l,1,p-1) + size$ + MID$(l,INSTR(l,"["))
  END IF

  IF (INSTR(l,",AL") OR INSTR(l," AL,")) AND INSTR(l,"byte[") THEN
    p=INSTR(l,"byte[") : l=MID$(l,1,p-1) + MID$(l,p+4)
  ELSEIF (INSTR(l,",AX") OR INSTR(l," AX,")) AND INSTR(l,"word[") THEN
    p=INSTR(l,"word[") : l=MID$(l,1,p-1) + MID$(l,p+4)
  END IF

  fixhexnums
END SUB

FUNCTION foundhexnum%
  digits$="0123456789" : hexdigits$=digits$ + "abcdefABCDEF"
  foundhexnum=FALSE

  lenl=LEN(l)
  FOR p = 1 TO lenl
    IF INSTR(digits$,MID$(l,p,1)) AND (p < lenl) THEN
      hexbegin=p : p=p+1 : p2=p
      DO WHILE INSTR(hexdigits$,MID$(l,p2,1)) AND (p2 < lenl) : p2=p2+1 : LOOP
      IF MID$(l,p2,1)="h" THEN
        hexend=p2-1
        foundhexnum=NOT FALSE
        EXIT FUNCTION
      END IF
    END IF
  NEXT p
END FUNCTION

SUB fixhexnums
  DO WHILE foundhexnum
    l=MID$(l,1,hexbegin-1) + "0x" + MID$(l,hexbegin,hexend-hexbegin+1) + _
      MID$(l,hexend+2)
  LOOP
END SUB

' <EOF>
