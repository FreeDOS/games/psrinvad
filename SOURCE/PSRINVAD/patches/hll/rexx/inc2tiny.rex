/* REXX */

signal on notready; signal on syntax

call time 'r'

parse version interp ver; say; say interp ver; say
call init

if lines(incfile)=1 then do while lines(incfile) \= 0
  call grabinc
end
else do lines(incfile)
  call grabinc
end

if lines(srcfile)=1 then do while lines(srcfile) \= 0
  call grab
end
else do lines(srcfile)
  call grab
end

say; say 'Elapsed:' format(time('e'),,2) 'secs.'; say
exit

init:
  srcfile='inv-nasm.asm'; incfile=delstr(srcfile,pos('.',srcfile))'.inc'
  outfile='inv-tiny.asm'
  if lines(srcfile) > 0 then say srcfile '->' outfile  /* notready? */
return

grabinc:
  line=linein(incfile)
  if word(line,3) = 'B' then do
    w=substr(word(line,2),3,length(word(line,2))-2)
    bytenames.w = 1
  end
return

grab:
  line=linein(srcfile); if length(line)=0 | found('%') then return

  if datatype(left(word(line,1),1),'u') & ,
    ((word(line,2)='DB') | (word(line,2)='DW')) then
     line=insert(':',line,pos(' ',line)-1)

  if found('+BX') then do
    line=delstr(line,p,3)
    parse var line before '[' after; line=before'[BX+'after
  end

  if found('[') & translate(substr(line,p+2,2))='S:' then do
    parse var line before '[' myseg ':' after
    line=translate(myseg) before'['after
  end

  if found('LEA ') then line=overlay('MOV',line,p,3)

  ofs='OFFSET'; lenofs=length(ofs)
  if found(',O') & translate(substr(line,p+1,lenofs))=ofs then
    line=delstr(line,p+1,lenofs+1)

  if found('s_') then do
    b=p; b2=pos('[',line); n=substr(line,b+2,b2-b-2)
    if bytenames.n = 1 then size='byte'; else size='word'
    line=substr(line,1,b-1) || size || substr(line,b2)
  end

  if (found(',AL') | found(' AL,')) & found('byte[') then
    line=delstr(line,p,4)
  else if (found(',AX') | found(' AX,')) & found('word[') then
    line=delstr(line,p,4)

  call fixhexnums

  call lineout outfile,line
return

foundhexnum:
  len=length(line); p=1
  do while p <= len
    if (datatype(substr(line,p,1),'n') & (p < len)) then do
      hexbegin=p; p2=p+1
      do while (datatype(substr(line,p2,1),'x') & (p2 < len))
        p2=p2+1
      end
      if substr(line,p2,1)='h' then do
        hexend=p2-1
        return 1
      end
    end
    p=p+1
  end
return 0

fixhexnums:
  do while foundhexnum()
    line=delstr(line,hexend+1,1); line=insert("0x",line,hexbegin-1)
  end
return

found: procedure expose line p
  p=pos(arg(1),line)
return min(1,p)

syntax:
notready:
  if interp \== 'brexx' then say condition('c')
  say '*** Error on line #'sigl ':'; say '"'sourceline(sigl)'"'; say
return

/* EOF */
