#n
# Quercus REXX DOS demo limits to 50 lines, so tidy!
#
# REXX/Personal 4.00 21 Mar 1992
#
# (for INV.REX, 3976 bytes)

N
N
N
N
s/ *\n/;/g
s/,;//g

# BUG / QUIRK: Quercus won't split words at hard tabs!
/linein(srcfile)/s//&;do while pos('9'x,line)>0;line=change('9'x,line,' ');end/

/\/\* *EOF *\*\//s/.*/change: procedure\
parse arg before,line,after;ofs=pos(before,line)\
if ofs \\= 0 then do;line=delstr(line,ofs,length(before))\
line=insert(after,line,ofs-1,length(after));end;return line/

w newinv.rex
