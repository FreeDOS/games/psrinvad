/* REXX */

signal on notready; signal on syntax; signal on novalue

call time 'r'

parse version interp ver; say; say interp ver; say
parse arg fasm; call init

if lines(srcfile)=1 then do while lines(srcfile) \= 0
  call grab
end
else do lines(srcfile)
  call grab
end

say; say 'Elapsed:' format(time('e'),,2) 'secs.'; say

say 'assemble  :' assemble
say '.COM size : 9,194 bytes'
say 'CRC32     : FFF22EF9'
say

exit

init:
  srcfile='INVADERS.ASM'; lineno=0; cseg=0

  if fasm='' then do
    outfile='inv-nasm.asm'; incfile=delstr(outfile,pos('.',outfile))'.inc'
    assemble='nasm -o inv-nasm.com' outfile '-O3'
  end
  else do
    outfile='inv-fasm.asm'; assemble='fasm' outfile
  end

  if lines(srcfile) > 0 then say srcfile '->' outfile  /* notready? */

  if fasm='' then do
    call writeout '%idefine offset'; call writeout '%define LEA MOV'
    call writeout '%define B byte';  call writeout '%define W word'
    call writeout '%include "'incfile'"'
  end
  else do
    call writeout 'LEA equ MOV'
    call writeout 'OFFSET equ'; call writeout 'Offset equ'
  end
return

grab:
  line=linein(srcfile); lineno=lineno+1

  if found(';') then line=delstr(line,p); if \found('"') then line=space(line)
  if length(line)=0 | found('CODE_SEG') | found('END') then return

  s='[0]'; if found(s) then line=delstr(line,p,length(s))
  if word(line,2)='DD' then line=word(line,1) 'DW 0,'subword(line,3)

  if word(line,2)='DB' | word(line,2)='DW' then do
    if fasm='' then
      call lineout incfile,'%define s_'word(line,1) right(word(line,2),1)
    call writeout line
    return
  end
  else if translate(right(word(line,2),7))=',OFFSET' | ,
      wordpos('LEA',line) > 0 then do
    call writeout line
    return
  end

  s='Word Ptr '; if found(s) then line=delstr(line,p,length(s))
  if found(' PROC ') then line=strip(delstr(line,p),'t')':'

  label=''; if right(word(line,1),1)=':' then parse var line label ':' line
  if label \= '' then call writeout label':'

  if label='RemoveNewInt9' | word(line,1)='CLC' then cseg = \cseg

  if length(line) > 0 then
    if words(line) > 1 then
      call writeout fixwords(line,cseg,fasm)
    else
      call writeout strip(line)
return

fixwords: procedure
  parse arg txt,seg,flatasm

  if pos(',',txt) > 0 then parse var txt w.1 w.2 ',' w.3 w.4
  else parse var txt w.1 w.2 w.3 w.4
  w.0 = words(w.1 w.2 w.3 w.4)
  if left(w.3,4)='ES:[' then w.3=overlay('[ES:',w.3,1,4)

  writeme=''
  do n=1 to w.0
    s='40:'; p=pos(s,w.n); if p > 0 then w.n=delstr(w.n,p,length(s))
    if flatasm='' & pos('+BX',w.n) > 0 then parse var w.n '[' w.n ']'

    parse var w.n . '[' digit ']'
    if right(w.n,1)=']' & datatype(digit)='NUM' then
      w.n=delstr(w.n,length(w.n)-2)'+'digit

    if (n > 1) & (length(w.n) > 2) & left(w.1,1) \= 'J' & ,
      wordpos(w.1,'CALL LOOP')=0 & datatype(left(w.n,1),'u') ,
    then do
      if seg then override='cs:'; else override=''
      if flatasm='' then do
        writeme=writeme's_'w.n'['override || w.n']'
        p=pos('+',writeme)
        if p > 0 then writeme=delstr(writeme,p,pos('[',writeme)-p)
      end
      else writeme=writeme'['override || w.n']'
    end
    else writeme=writeme || w.n

    if n=1 & w.0 > 1 then writeme=writeme' '
    else if n < w.0 then writeme=writeme','
  end
return writeme

found: procedure expose line p
  p=pos(arg(1),line)
return min(1,p)

writeout: procedure expose outfile
  call lineout outfile,arg(1)
return

syntax:
notready:
  if interp \== 'brexx' then say condition('c')
  say '*** Error on line #'sigl ':'; say '"'sourceline(sigl)'"'; say
return

novalue:
  say '!! NOVALUE !!' sigl ':' sourceline(sigl)
return

/* EOF */
