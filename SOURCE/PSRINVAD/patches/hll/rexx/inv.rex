/* REXX */

signal on notready; signal on syntax

call time 'r'

parse version interp ver; say; say interp ver; say
call init; say; say '=== initialized'

if lines(srcfile)=1 then do while lines(srcfile) \= 0
  call grab
end
else do lines(srcfile)
  call grab
end
say '=== grabbed'

call adjust; say '=== adjusted'

/* tab='9'x */ tab=' =>'
say
say 'Works "out of the box":'
say tab 'jwasm -Zm -bin -Fo=inv-jwas.com inv.asm'
say tab 'a86 +S inv.asm inv-a86'
say tab 'tasm inv && tlink /t/x inv,inv-tasm'
say tab 'wasmr inv && wlink format dos com file inv name inv-wat'
say tab tab '(or "warplink /c inv,whatever")'

say; say 'Elapsed:' format(time('e'),,2) 'secs.'; say
exit

adjust:
  do num=1 to src.0
    line=src.num

    call finddel '[0]'; call finddel 'Word Ptr '

    if found('+BX') then do
      p2=pos('[',line)
      w=substr(line,p2+1,p-p2-1)
      if bytenames.w=1 then size='byte'; else size='word'
      line=insert(size' ptr',line,p2-1)
    end
    else if found('LEA ') then do
      line=overlay('MOV ',line,p)
      line=insert('OFFSET ',line,pos(',',line))
    end
    else if found(' PROC ') & found(' ') then do
      line=insert(':',line,p-1); line=delstr(line,p+1)
    end
    else if found('JMP ') then call jmpshort

    data='DB DW'
    if \(wordpos(word(line,1),data) > 0 | ,
         wordpos(word(line,2),data) > 0) ,
         & found(',') ,
    then do
      do i=1 to words(line)
        w=word(line,i)
        if pos(',',w) > 0 then do
          parse var w before ',' after
          if translate(after) \= 'OFFSET' then
          if foundid(before) then call fixid before
          else if foundid(after) then call fixid after
        end
      end
    end
    else if wordpos('INC',line) > 0 | wordpos('DEC',line) > 0 then do
      n=words(line)
      if wordlength(line,n) > 2 & \found('+BX') then do
        w=fixbrakdig(word(line,n))
        line=delword(line,n) size' ptr['w || plus']'
      end
    end

    if wordpos(word(line,1),'RemoveNewInt9: CLC') > 0 then cseg = \cseg
    if cseg then call insertseg

    call lineout outfile,line
  end
return

init:
  srcfile='INVADERS.ASM'; lineno=0; cseg=0; jmp=0; src.0=0
  outfile='inv.asm'

  if lines(srcfile) > 0 then say srcfile '->' outfile  /* notready? */

  noshort='#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\`abcdefy'
return

grab:
  line=linein(srcfile)

  if found(';') then line=delstr(line,p)
  else if word(line,1)='Assume' | word(line,2)='ENDP' then line=''

  if length(line)=0 then return

  if found('"') then do
    parse var line before 'DB' '"' after '"' rest
    line=strip(before) 'DB "'after'"'rest
  end
  else line=space(line)

  if word(line,2)='DB' then do; w=word(line,1); bytenames.w=1; end

  lineno=lineno + 1
  src.lineno = line; src.0=src.0 + 1
return

jmpshort: procedure expose line jmp noshort p
  jmp=jmp+1
  if pos(d2c(jmp+c2d(' ')),noshort)=0 then
    line=insert('SHORT ',line,p+3)
return

insertseg: procedure expose line
  if found('MOV ') then do
    if found('40:') then line=overlay('DS:',line,p)

    if found('MOV ') & found('[') & \found('[0') then
      line=insert(' cs:',line,pos('[',line)-1)
  end
return

foundid: procedure
return datatype(left(arg(1),1),'u') & length(arg(1)) > length('DX') & ,
       substr(arg(1),3,1) \= ':'

fixbrakdig: procedure expose bytenames. plus size
  s=arg(1); plus=''
  if right(s,1)=']' & substr(s,length(s)-2,1)='[' then do
    parse var s s '[' digit ']'
    plus='+'digit
  end
  if bytenames.s=1 then size='byte'; else size='word'
return s

fixid: procedure expose bytenames. line
  call finddel arg(1)
  w=fixbrakdig(arg(1))
  line=insert(size' ptr['w || plus']',line,p-1)
return

found: procedure expose line p
  p=pos(arg(1),line)
return min(1,p)

finddel: procedure expose line p
  if found(arg(1)) then line=delstr(line,p,length(arg(1)))
return

syntax:
notready:
  if interp \== 'brexx' then say condition('c')
  say '*** Error on line #'sigl ':'; say '"'sourceline(sigl)'"'; say
return

/* EOF */
