/* REXX */

signal on notready; signal on syntax; signal on novalue

call time 'r'

parse version interp ver; say; say interp ver; say
call init

if lines(srcfile)=1 then do while lines(srcfile) \= 0
  call grab
end
else do lines(srcfile)
  call grab
end

say; say 'Elapsed:' format(time('e'),,2) 'secs.'; say

say 'assemble  :' assemble
say '.COM size : 9,194 bytes'
say 'CRC32     : FFF22EF9'
say

exit

init:
  srcfile='INVADERS.ASM'; lineno=0; cseg=0; jumpnum=0
  outfile='inv-wolf.asm'; assemble='wasm.com' outfile

  if lines(srcfile) > 0 then say srcfile '->' outfile  /* notready? */

  noshort="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\`abcdefy"
return

grab:
  line=linein(srcfile); lineno=lineno+1

  if left(line,1)=';' | length(line)=0 then do
    call writeout line; return
  end

  if found('CODE_SEG') | found('END ') then return

  if word(line,2)='DD' then line=word(line,1) 'DW 0,'subword(line,3)

  if word(line,2)='DB' then
    do while found('"'); line=overlay("'",line,p,1); end

  s='[0] 40: Ptr'
  do n=1 to words(s)
    w=word(s,n); if found(w) then line=delstr(line,p,length(w))
  end

  if word(line,2)='ENDP' then line=' 'delword(line,1,1)
  if datatype(left(line,1),'u') & found(':') then line=overlay(' ',line,p,1)

  parse var line before '[' digit ']' after
  if length(digit)=1 & datatype(digit)='NUM' then
    line=before'+'digit || after

  s='LEA '
  if found(s) then do
    line=overlay('MOV ',line,p,length(s))
    line=insert('OFFSET ',line,pos(',',line))
  end

  if found('SHL') | found('SHR') then line=delstr(line,pos(',1',line),2)

  if (found('CMP ') | found('MOV ') | found('INC ')) & ,
    (found('BombY') | found('BombT')) then do
        p=pos('[',line)-1
        if p > 0 then line=insert('byte',line,p)
      end

  s='ES:'; p=pos(s'[',line)
  if p > 0 then do
    line=delstr(line,p,length(s))
    call writeout ' 's
  end

  if found('JMP ') then call jmpshort

  if (word(line,1)='RemoveNewInt9' & word(line,2)='PROC') | ,
    word(line,1)='CLC' then
      cseg = \cseg
  if cseg & found('MOV ') & (found('StoreAX') | found(',W') | ,
      right(line,2)=',0' | right(line,2)=',1') then
        call writeout ' CS:'

  call writeout line
return

jmpshort: procedure expose line jumpnum noshort
  jumpnum=jumpnum+1
  if pos(d2c(jumpnum+c2d(' ')),noshort)=0 then
    line=insert('S',line,pos('JMP ',line)+2)
return

found: procedure expose line p
  p=pos(arg(1),line)
return min(1,p)

writeout: procedure expose outfile
  call lineout outfile,arg(1)
return

syntax:
notready:
  if interp \== 'brexx' then say condition('c')
  say '*** Error on line #'sigl ':'; say '"'sourceline(sigl)'"'; say
return

novalue:
  say '!! NOVALUE !!' sigl ':' sourceline(sigl)
return

/* EOF */
