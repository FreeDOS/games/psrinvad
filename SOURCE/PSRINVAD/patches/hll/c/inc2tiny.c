/* public domain, nenies proprajho, free for any use */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXLINE 150
#define MAXBYTENAMES 60
#define MAXBYTENAMELEN 19
#define LEN strlen(line)
#define LAST line[LEN-1]
#define findchar(c) strchr(line,c)
#define found(s) ((p=strstr(line,s)) != NULL)
#define foundchar(c) ((p=findchar(c)) != NULL)
#define insert(s,at) do { memmove(at+strlen(s),at,strlen(at)+1);\
                          strncpy(at,s,strlen(s)); }\
                     while(0)
#define delete(s,n) memmove(s,s+n,strlen(s+n)+1)
#define DELETECRLF while ((LAST=='\n')||(LAST=='\r')) delete(&LAST,1)

char line[MAXLINE],bytenames[MAXBYTENAMES][MAXBYTENAMELEN+1],
     *p,*p2,*hexbegin,*hexend;

int getbytenames(void) {
  FILE *incfile; int bytenum=0,rc=0;

  if ((incfile=fopen("inv-nasm.inc","r")) != NULL) {
    do {
      if (fgets(line,sizeof(line),incfile) != NULL) {
        DELETECRLF;
        if (LAST=='B') {
          delete(line,10); delete(line+strlen(line)-2,2);
          strcpy(bytenames[bytenum++],line);
        }
      }
    } while (!feof(incfile));
    fclose(incfile);
    rc=!0;
  }
  return rc;
}

int isbytename(char* n) {
  int i;

  for (i=0; i < MAXBYTENAMES; i++)
    if (strcmp(bytenames[i],n)==0)
      return !0;

  return 0;
}

int foundhexnum(void) {
  for (p=line; p <= &LAST; p++)
    if (isdigit(*p) && (p < &LAST)) {
      hexbegin=p; p2=p+1;
      while (isxdigit(*p2) && (p2 < &LAST)) p2++;
      if (*p2=='h') {
        hexend=p2-1;
        return !0;
      }
    }

  return 0;
}

void fixhexnums(void) {
  while (foundhexnum()) {
    delete(hexend+1,1); insert("0x",hexbegin);
  }
}

void Adjust(void) {
  char tmp[MAXLINE],size[]="word"; int l=0;

  if (isalpha(line[0]) && (found(" DB ") || found(" DW ")))
    insert(":",findchar(' '));
  else if (found("+BX")) {
    delete(p,3); insert("BX+",findchar('[')+1);
  }
  else if (foundchar('[') && ((*(p+3)==':') && (toupper(*(p+2))=='S'))) {
    strcpy(tmp,"?S "); tmp[0]=toupper(*(p+1));
    delete(p+1,3); insert(tmp,line);
  }
  else if (found("LEA "))
    strncpy(p,"MOV",3);
  else if (found(",O") && toupper(*(p+2))=='F')
    delete(p+1,7);

  if (found("s_")) {
    p += 2; p2 = findchar('['); strcpy(tmp,line);
    delete(p2,strlen(p2)); delete(line,(int)(p-line)); l=LEN;
    if (isbytename(line)) strcpy(size,"byte");
    strcpy(line,tmp); insert(size,p2); delete(p-2,l+2);
  }

  if (((found(",AL") || found(" AL,")) && found("byte[")) ||
      ((found(",AX") || found(" AX,")) && found("word[")))
        delete(p,4);

  fixhexnums();
}

int main(void) {
  int rc=EXIT_FAILURE; FILE *oldasm,*newasm;

  if (getbytenames() && (oldasm=fopen("inv-nasm.asm","r")) != NULL) {
    newasm=fopen("inv-tiny.asm","w");
    do {
      if (fgets(line,sizeof(line),oldasm) != NULL) {
        DELETECRLF;
        if ((LEN > 0) && (line[0] != '%')) {
          if (line[0] != ';') Adjust();
          fputs(line,newasm); fputs("\n",newasm);
        }
      }
    } while (!feof(oldasm));
    fclose(oldasm); fclose(newasm);
    rc=EXIT_SUCCESS;
  }
  return rc;
}

/* EOF */
