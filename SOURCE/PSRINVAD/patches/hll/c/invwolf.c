#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STRMAX 150
#define LEN strlen(line)
#define LASTCHAR line[LEN-1]
#define found(s) ((p=strstr(line,s)) != NULL)
#define foundstart(s) (strncmp(s,line,strlen(s))==0)
#define foundend(s) (strncmp(s,&line[LEN-strlen(s)],strlen(s))==0)

char line[STRMAX],*p; FILE *oldasm,*newasm;

void shortjmp(void) {
  static char noshort[]="#$%&,.134578>DGJMOPQRSTUVWXYZ][_^\\`abcdefy";
  static int num=0; char tmp[STRMAX];

  if (!strchr(noshort,++num+' ')) {
    sprintf(tmp,"%.*sS%s",(int)(p-line)+3,line,p+3);
    strcpy(line,tmp);
  }
}

void insertseg(void) {
  static int cseg=0;

  if (foundstart("RemoveNewInt9")) cseg=!0;
  if (cseg && found("MOV "))
    if (found(",W") || found("StoreAX") ||
      ((line[LEN-2]==',') &&
      ((LASTCHAR=='0') || (LASTCHAR=='1'))))
        fprintf(newasm," CS:\n");
  if (foundend("CLC")) cseg=0;
}

void Adjust(void) {
  char tmp[STRMAX];

  if ((LEN==0) || (line[0]==';')) goto writeline;
  else if (found("CODE_SEG") || found("END ")) return;

  if (found(" DB ")) while (found("\"")) *p='\'';
  else if ((found("SHL") || found("SHR")) && (found(",1"))) strncpy(p,"  ",2);
  else if (found(" Ptr")) strncpy(p,"    ",4);
  else if (found("40:"))  strncpy(p,"   ",3);

  if (found("[")) {
    if ((*(p+2)==']') && isdigit(*(p+1))) {
      sprintf(tmp,"%.*s+%c%s",(int)(p-line),line,*(p+1),p+3);
      strcpy(line,tmp);
    }
  }

  if (found(" DD ")) {
    sprintf(tmp,"%.*s DW 0,%s",(int)(p-line),line,p+4);
    strcpy(line,tmp);
  }

  if (found(" ENDP")) { strcpy(tmp,p); strcpy(line,tmp); }
  else if (isupper(line[0]) && found(":")) *p=' ';
  else if (found("ES:[")) { strncpy(p,"   ",3); fprintf(newasm," ES:\n\n"); }

  if (found("CMP ") || found("MOV ") || found("INC "))
    if (found("BombY") || found("BombT")) {
      (void)found("[");
      sprintf(tmp,"%.*sbyte[%s",(int)(p-line),line,p+1);
      strcpy(line,tmp);
    }

  if (found("LEA ")) {
    strncpy(p,"MOV",3);
    (void)found(",");
    sprintf(tmp,"%.*s,OFFSET %s",(int)(p-line),line,p+1);
    strcpy(line,tmp);
  }

  if (found("JMP ")) shortjmp(); insertseg();

writeline:
  fprintf(newasm,"%s\n",line);
}

int main(int argc,char** argv) {
  oldasm=fopen((argc > 1) ? argv[1] : "INVADERS.ASM","r");

  if (oldasm == NULL) {
      fputs("\nFile not found!\n",stderr);
      return EXIT_FAILURE;
  }

  newasm=fopen("inv-wolf.asm","w");

  do {
    fgets(line,sizeof(line),oldasm);
    while ((LASTCHAR=='\n') || (LASTCHAR=='\r')) LASTCHAR='\0';
    if (feof(oldasm)) break;
    Adjust();
  } while (!feof(oldasm));

  fclose(newasm); fclose(oldasm);

  return EXIT_SUCCESS;
}

/* EOF */
