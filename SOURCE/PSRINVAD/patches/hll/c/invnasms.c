/*
  minor variation for SUBC-DOS [2014] (t3x.org)

  (extra credit): not using bloated printf()

  public domain, so don't complain!
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STRMAX 150
#define INCFILE "inv-nasm.inc"
#define LEN strlen(line)
#define LASTCHAR line[LEN-1]
#define WRITELINE fputs(line,newasm); fputs("\n",newasm)

char line[STRMAX],*p,*p2; FILE *oldasm,*newasm,*incfile;

int found(char *s) {
  int slen;

  slen=strlen(s);
  for (p=strchr(line,s[0]); (p != NULL) && (p < line+LEN-slen+1); p++)
    if (strncmp(p,s,slen)==0)
      return (p != NULL);

  p=NULL;
  return 0;
}

int foundstart(char *s) {
  return (strncmp(s,line,strlen(s))==0);
}

int foundend(char *s) {
  return (strncmp(s,&line[LEN-strlen(s)],strlen(s))==0);
}

void insert(char *s,char *at) {
  memmove(at+strlen(s),at,strlen(at)+1); strncpy(at,s,strlen(s));
}

void delete(char *s,int n) {
  memmove(s,s+n,strlen(s+n)+1);
}

void finddel(char *s) {
  if (found(s)) delete(p,strlen(s));
}

void fixproc(void) {
  if (found(" PROC ") && (found(" "))) { *p=':'; *(p+1)='\0'; }
}

void incdec(void) {
  char *p0,tmp[STRMAX]; int n;

  if (found("INC ") || found("DEC ")) {
    p += 4; while (*p == ' ') p++;

    if (isupper(*p)) {
      p0=p;
      if (found(";")) p2=p-1; else p2=&LASTCHAR;
      p=p0;
      while (*p2 == ' ') p2--;

      n=(int)(p2-p)+1; strncpy(tmp,p,n); tmp[n]='\0';

      if (strlen(tmp) > 2) {
        insert("]",p2+1); insert("[",p);
        insert(tmp,p); insert("s_",p);
      }
    }
  }
}

void fixop1(void) {
  char tmp[STRMAX]; int n;

  if (found(",") && (*(p-1) != ']')) {
    p--; p2=p;
  
    do {
      p--;
    } while ((*p != ' ') && (isalnum(*p) || (*p == '_')));
    p++;

    n=(int)(p2-p)+1; strncpy(tmp,p,n); tmp[n]='\0';

    if ((strlen(tmp) > 2) && (*p != '[') && isupper(*p)) {
      insert("]",p2+1); insert("[",p);
      insert(tmp,p); insert("s_",p);
    }
  }
}

void fixop2(void) {
  char tmp[STRMAX]; int n;

  if (found(";")) {
    *p='\0'; p2=p; (void)found(","); *p2=';';
  }
  else (void)found(",");

  if ((p != NULL) && (*p != '[')) {
    p++; p2=p;

    while (((int)(p2-line) < (int)LEN-1) && (*p2 != ' ') &&
      (isalnum(*p2) || (*p2 == '_')))
        p2++;

    while (*p2 == ' ') p2--;

    n=(int)(p2-p)+1; strncpy(tmp,p,n); tmp[n]='\0';

    if ((strlen(tmp) > 2) && isupper(*p)) {
      insert("]",p2+1); insert("[",p);
      insert(tmp,p); insert("s_",p);
    }
  }
}

void fixbrakdig(void) {
  if (found("[") && (*(p-2)=='_') && isdigit(*(p-1))) delete(p-2,2);
  if (found("]") && (*(p-2)=='_') && isdigit(*(p-1))) *(p-2)='+';
}

void unbrakdig(void) {
  if (found("[") && (*(p+2) == ']') && isdigit(*(p+1))) {
    *p='_'; finddel("]");
  }
}

void fixplusbx(void) {
  char tmp[STRMAX]; int n;

  if (found("+BX")) {
    if (found("[")) p2=p; if (found("+")) p--;

    n=(int)(p-p2); strncpy(tmp,p2+1,n); tmp[n]='\0';
    insert(tmp,p2); insert("s_",p2);
  }
}

void fixseg(void) {
  static int cseg=0;

  if (foundstart("RemoveNewInt9")) cseg=!0;
  if (cseg && found("MOV ") && (!found("[0")) && found("["))
    insert("cs:",p+1);
  if (foundend("CLC")) cseg=0;
}

int writeinc(void) {
  char tmp[STRMAX]; int n,rc=0;

  if (isupper(line[0]) && (found(" DB ") || found(" DW "))) {
    (void)found(" "); p2=p; (void)found(" D");
    n=(int)(p2-line); strncpy(tmp,line,n); tmp[n]='\0';
    insert("%define s_",tmp);
    strcat(tmp," ?"); tmp[strlen(tmp)-1]=*(p+2); strcat(tmp,"\n");
    fputs(tmp,incfile); WRITELINE;
    rc=!0;
  }
  return rc;
}

void Adjust(void) {
  if ((LEN==0) || (line[0]==';') || found("LEA ")) { WRITELINE; return; }
  else if (found("CODE_SEG") || found("END")) return;

  if (found(" DD ")) { delete(p,4); insert(" DW 0,",p); }
  if (writeinc()) return; finddel("[0]");

  if (!found(",O")) { /* ",O[fF][sS][eE][tT]" */
    if (found("ES:[")) strncpy(p,"[ES:",4);
    finddel("40:"); finddel("Word Ptr ");
    fixproc(); unbrakdig(); fixplusbx();
    fixop1(); fixop2(); incdec();
    fixbrakdig(); fixseg();
  }
  WRITELINE;
}

int main(int argc,char** argv) {
  oldasm=fopen((argc > 1) ? argv[1] : "INVADERS.ASM","r");

  if (oldasm == NULL) {
    fputs("\nNot found!\n",stderr);
    return EXIT_FAILURE;
  }

  newasm=fopen("inv-nasm.asm","w"); incfile=fopen(INCFILE,"w");

  fputs("%idefine offset\n%define LEA MOV\n"
        "%define B byte\n%define W word\n"
        "%include \"" INCFILE "\"\n",newasm);

  do {
    fgets(line,sizeof(line),oldasm);
    while ((LASTCHAR=='\n') || (LASTCHAR=='\r')) LASTCHAR='\0';
    if (feof(oldasm)) break;
    Adjust();
  } while (!feof(oldasm));

  fclose(incfile); fclose(newasm); fclose(oldasm);

  return EXIT_SUCCESS;
}

/* EOF */
