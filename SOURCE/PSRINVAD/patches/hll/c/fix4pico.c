// avoiding dependency on Sed

#include <stdio.h>
#include <string.h>

#define LASTCHAR line[strlen(line)-1]
#define READLINE fgets(line,sizeof(line),OLD)
#define WRITELINE fputs(line,NEW)
#define NL fputs("\n",NEW)

char line[80],*p,*p2; FILE *OLD,*NEW;

int found(char *s) { return ((p=strstr(line,s)) != NULL); }

void whatever(char *str) {
  *p='\0'; fputs(str,NEW); fputs("if (",NEW); p += 6; p2=strchr(p,')');
  *p2='\0'; fputs(p,NEW); fputs("))",NEW); p2++;
}

void fix(void) {
  if (found("delete(")) {
    *p='\0'; WRITELINE; fputs("del(",NEW);
    fputs(p+7,NEW); NL;
  }
  else if (found("p2-line")) {
    *(p+3)='\0'; WRITELINE; fputs("&line[0]",NEW);
    fputs(p+7,NEW); NL;
  }
  else if (found("EXIT_")) {
    fputs("return ",NEW);
    fputs(found("SUCCESS") ? "0;\n" : "1;\n",NEW);
  }
  else if (found("#define WRITELINE") && (p==&line[0])) {
    p=strchr(line,';'); *p='\0'; WRITELINE; NL; *p=';';
    fputs("#define WRITENL",NEW); fputs(p+1,NEW); NL;
  }
  else if (found("WRITELINE;")) {
    *(p+10)='\0'; WRITELINE; fputs("WRITENL;",NEW);
    fputs(p+11,NEW); NL;
  }
  else if (found("int found(") && (p==&line[0])) {
    do READLINE; while (!found("}"));
    fputs("int found(char *s){return((p=strstr(line,s))!=NULL);}\n",NEW);
  }
  else if (found("%idefine")) {
    do READLINE; while (!found("%include"));
    fputs("fputs(\"%idefine offset\\n%define LEA MOV\\n",NEW);
    fputs("%define B byte\\n%define W word\\n",NEW);
    fputs("%include \\\"inv-nasm.inc\\\"\\n\",newasm);",NEW);
  }
  else if (found("(void)found(")) {
    whatever(line); if ((p=strstr(p2,"(void)found(")) != NULL) whatever(p2);
    fputs(p2,NEW); NL;
  }
  else { WRITELINE; NL; }
}

int main(void) {
  int rc=0;
  if ((OLD=fopen("invnasms.c","r")) != NULL) {
    NEW=fopen("invnasms.new","w");
    do {
      READLINE;
      while ((LASTCHAR=='\n') || (LASTCHAR=='\r')) LASTCHAR='\0';
      if (feof(OLD)) break;
      fix();
    } while (!feof(OLD));
    fclose(OLD); fclose(NEW);
  }
  else { fputs("Oops!",stderr); rc=1; }
  return rc;
}
