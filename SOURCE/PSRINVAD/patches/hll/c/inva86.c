#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STRMAX 135
#define INCFILE "inv.inc"
#define LEN strlen(line)
#define LASTCHAR line[LEN-1]
#define found(s) ((p=strstr(line,s)) != NULL)
#define foundstart(s) (strncmp(s,line,strlen(s))==0)
#define foundend(s) (strncmp(s,&line[LEN-strlen(s)],strlen(s))==0)

char line[STRMAX],*p; FILE *oldasm,*incfile,*newasm;

void Adjust(void) {
  char tmp[STRMAX]; size_t n;
  static char noshort[]="#$%&,./123456789=>DGJMOPQRSTUVWXYZ[\\]^_`abcdefy";
  static int cseg=0,jumpnum=0;

  if ((LEN==0) || (line[0]==';') || found("CODE_SEG") || found("END"))
       goto writeline;

  if (isupper(line[0]) && (found(" DB ") || found(" DW "))) {
      n=(size_t)(strchr(line,' ')-line);
      strncpy(tmp,line,n); tmp[n]='\0';
      (void)found(" D");
      fprintf(incfile,"EXTRN %s:%c\n",tmp,*(p+2));
  }

  if ((LEN >= 3) && (strncmp(&line[LEN-3],"[0]",3)==0))
       line[LEN-3]='\0';
  else if (found("40:")) strncpy(p,"DS",2);

  if (foundstart("RemoveNewInt9")) cseg=!0;
  if (cseg && found("MOV "))
    if (found(",W") || found("StoreAX") ||
       (strncmp(&line[LEN-2],",0",2)==0) ||
       (strncmp(&line[LEN-2],",1",2)==0)) {
        sprintf(tmp,"cs %s",line);
        strcpy(line,tmp);
    }
  if (foundend("CLC")) cseg=0;

  if (found("JMP ") && (!strchr(noshort,++jumpnum+' '))) {
      sprintf(tmp,"%.*sSHORT %s",(int)(p-line)+4,line,p+4);
      strcpy(line,tmp);
  }

writeline:
  fprintf(newasm,"%s\n",line);
}

int main(int argc,char** argv) {
  oldasm=fopen((argc > 1) ? argv[1] : "INVADERS.ASM","r");

  if (oldasm == NULL) {
      fputs("\nFile not found!\n",stderr);
      return EXIT_FAILURE;
  }

  incfile=fopen(INCFILE,"w"); newasm=fopen("inv.a86","w");
  fprintf(newasm,"include %s\n",INCFILE);

  do {
    fgets(line,sizeof(line),oldasm);
    while ((LASTCHAR=='\n') || (LASTCHAR=='\r')) LASTCHAR='\0';
    if (feof(oldasm)) break;
    Adjust();
  } while (!feof(oldasm));

  fclose(newasm); fclose(incfile); fclose(oldasm);

  return EXIT_SUCCESS;
}

/* EOF */
