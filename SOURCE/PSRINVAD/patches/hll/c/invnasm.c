#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STRMAX 150
#define INCFILE "inv-nasm.inc"
#define LEN strlen(line)
#define LASTCHAR line[LEN-1]
#define SKIPBLANK(n,z) while (*n == ' ') z
#define found(s) ((p=strstr(line,s)) != NULL)
#define foundstart(s) (strncmp(s,line,strlen(s))==0)
#define foundend(s) (strncmp(s,&line[LEN-strlen(s)],strlen(s))==0)
#define finddel(s) if(found(s))memmove(p,p+strlen(s),strlen(p+strlen(s))+1)

char line[STRMAX],*p,*p2; FILE *oldasm,*newasm,*incfile;

void incdec(void) {
  char *p0,tmp[STRMAX],tmp2[STRMAX];

  if (found("INC ") || found("DEC ")) {
    p += 4; SKIPBLANK(p,p++);

    if (isupper(*p)) {
      p0=p;
      if (found(";")) p2=p-1; else p2=&LASTCHAR;
      p=p0;

      SKIPBLANK(p2,p2--);

      sprintf(tmp,"%.*s",(int)(p2-p+1),p);

      if (strlen(tmp) > 2) {
        sprintf(tmp2,"%.*ss_%s[%s]",(int)(p-line),line,tmp,tmp);
        strcpy(line,tmp2);
      }
    }
  }
}

void fixop1(void) {
  char tmp[STRMAX],tmp2[STRMAX];

  if (found(",")) {
    p2=p;

    if (*(p2-1) != ']') {
      p2--; p=p2;
  
      do {
        p--;
      } while ((*p != ' ') && (isalnum(*p) || (*p == '_')));
      p++;

      sprintf(tmp,"%.*s",(int)(p2-p)+1,p);

      if ((strlen(tmp) > 2) && (*p != '[') && isupper(*p)) {
        sprintf(tmp2,"%.*ss_%s[%s]%s",(int)(p-line),line,tmp,tmp,p2+1);
        strcpy(line,tmp2);
      }
    }
  }
}

void fixop2(void) {
  char tmp[STRMAX],tmp2[STRMAX];

  if (found(";")) {
    *p='\0'; p2=p; (void)found(","); *p2=';';
  }
  else (void)found(",");

  if ((p != NULL) && (*p != '[')) {
    p++; p2=p;

    while (((unsigned)(p2-line) < LEN-1) && (*p2 != ' ') &&
      (isalnum(*p2) || (*p2 == '_')))
        p2++;

    SKIPBLANK(p2,p2--);
    sprintf(tmp,"%.*s",(int)(p2-p)+1,p);

    if ((strlen(tmp) > 2) && isupper(*p)) {
      sprintf(tmp2,"%.*ss_%s[%s]%s",(int)(p-line),line,tmp,tmp,p2+1);
      strcpy(line,tmp2);
    }
  }
}

void fixbrakdig(void) {
  char tmp[STRMAX];

  if (found("[") && (*(p-2)=='_') && isdigit(*(p-1))) {
    sprintf(tmp,"%.*s%s",(int)(p-line)-2,line,p);
    strcpy(line,tmp);
  }
  if (found("]") && (*(p-2)=='_') && isdigit(*(p-1))) *(p-2)='+';
}

void fixplusbx(void) {
  char tmp[STRMAX];

  if (found("+BX")) {
    if (found("[")) p2=p; if (found("+")) p--;
    sprintf(tmp,"%.*ss_%.*s%s",(int)(p2-line),line,(int)(p-p2),p2+1,p2);
    strcpy(line,tmp);
  }
}

void fixseg(void) {
  static int cseg=0; char tmp[STRMAX];

  if (foundstart("RemoveNewInt9")) cseg=!0;
  if (cseg && found("MOV ") && (!found("[0")) && found("[")) {
    p++;
    sprintf(tmp,"%.*scs:%s",(int)(p-line),line,p);
    strcpy(line,tmp);
  }
  if (foundend("CLC")) cseg=0;
}

void Adjust(void) {
  char tmp[STRMAX];

  if ((LEN==0) || (line[0]==';') || found("LEA ")) goto writeline;
  else if (found("CODE_SEG") || found("END")) return;

  if (found(" DD ")) {
    sprintf(tmp,"%.*s DW 0,%s",(int)(p-line),line,p+4);
    strcpy(line,tmp);
  }

  if (isupper(line[0]) && (found(" DB ") || found(" DW "))) {
    (void)found(" "); p2=p; (void)found(" D");
    fprintf(incfile,"%%define s_%.*s %c\n",(int)(p2-line),line,*(p+2));
    goto writeline;
  }

  finddel("[0]");

  if (!found(",O")) /* ",O[fF][sS][eE][tT]" */ {
    if (found("ES:[")) strncpy(p,"[ES:",4);
    finddel("40:"); finddel("Word Ptr ");

    if (found(" PROC ")) {
      if (found(" ")) *p=':'; *(p+1)='\0';
    }

    if (found("[") && (*(p+2) == ']') && isdigit(*(p+1))) {
      *p='_'; finddel("]");
    }

    fixplusbx(); fixop1(); fixop2(); incdec(); fixbrakdig(); fixseg();
  }

writeline:
  fprintf(newasm,"%s\n",line);
}

int main(int argc,char** argv) {
  oldasm=fopen((argc > 1) ? argv[1] : "INVADERS.ASM","r");

  if (oldasm == NULL) {
      fputs("\nFile not found!\n",stderr);
      return EXIT_FAILURE;
  }

  newasm=fopen("inv-nasm.asm","w"); incfile=fopen(INCFILE,"w");

  fprintf(newasm,"%%idefine offset\n%%define LEA MOV\n"
          "%%define B byte\n%%define W word\n%%include \"%s\"\n",INCFILE);

  do {
    fgets(line,sizeof(line),oldasm);
    while ((LASTCHAR=='\n') || (LASTCHAR=='\r')) LASTCHAR='\0';
    if (feof(oldasm)) break;
    Adjust();
  } while (!feof(oldasm));

  fclose(incfile); fclose(newasm); fclose(oldasm);

  return EXIT_SUCCESS;
}

/* EOF */
