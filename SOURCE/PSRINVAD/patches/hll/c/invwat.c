#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STRMAX 150
#define INCFILE "inv-wat.inc"
#define LEN strlen(line)
#define LASTCHAR line[LEN-1]
#define SKIPBLANK(n,z) while (*n == ' ') z
#define found(s) ((p=strstr(line,s)) != NULL)
#define finddel(s) if(found(s))memmove(p,p+strlen(s),strlen(p+strlen(s))+1)

char line[STRMAX],*p,*p2; FILE *oldasm,*newasm,*incfile;

void incdec(void) {
  char *p0,tmp[STRMAX],tmp2[STRMAX];

  if (found("INC ") || found("DEC ")) {
    p += 4; SKIPBLANK(p,p++);

    if isupper(*p) {
      p0=p;
      if found(";") {
        p2=p-1;
      }
      else p2=&LASTCHAR;
      p=p0;
      SKIPBLANK(p2,p2--);

      sprintf(tmp,"%.*s",(int)(p2+1-p),p);

      if (strlen(tmp) > 2) {
        sprintf(tmp2,"%.*ss_%s[%s]",(int)(p-line),line,tmp,tmp);
        strcpy(line,tmp2);
      }
    }
  }
}

void fixop1(void) {
  char tmp[STRMAX],tmp2[STRMAX];

  if (found(",")) {
    p2=p;

    if (*(p2-1) != ']') {
      p2--; p=p2;
  
      do {
        p--;
      } while ((*p != ' ') && (isalnum(*p) || (*p == '_')));
      p++;

      sprintf(tmp,"%.*s",(int)(p2-p)+1,p);

      if ((strlen(tmp) > 2) && (*p != '[') && isupper(*p)) {
        sprintf(tmp2,"%.*ss_%s[%s]%s",(int)(p-line),line,tmp,tmp,p2+1);
        strcpy(line,tmp2);
      }
    }
  }
}

void fixop2(void) {
  char tmp[STRMAX],tmp2[STRMAX];

  if (found(";")) {
    *p='\0'; p2=p; (void)found(","); *p2=';';
  }
  else (void)found(",");

  if ((p != NULL) && (*p != '[')) {
    p++; p2=p;

    while (((unsigned)(p2-line) < LEN-1) && (*p2 != ' ') &&
      (isalnum(*p2) || (*p2 == '_')))
        p2++;

    SKIPBLANK(p2,p2--);
    sprintf(tmp,"%.*s",(int)(p2-p)+1,p);

    if ((strlen(tmp) > 2) && (tmp[2] != ':') && isupper(*p)) {
      sprintf(tmp2,"%.*ss_%s[%s]%s",(int)(p-line),line,tmp,tmp,p2+1);
      strcpy(line,tmp2);
    }
  }
}

void fixbrakdig(void) {
  char tmp[STRMAX];

  if (found("[") && (*(p-2)=='_') && isdigit(*(p-1))) {
    sprintf(tmp,"%.*s%s",(int)(p-line)-2,line,p);
    strcpy(line,tmp);
  }
  if (found("]") && (*(p-2)=='_') && isdigit(*(p-1))) *(p-2)='+';
}

void fixplusbx(void) {
  char tmp[STRMAX];

  if (found("+BX")) {
    if (found("[")) p2=p; if (found("+")) p--;
    sprintf(tmp,"%.*ss_%.*s%s",(int)(p2-line),line,(int)(p-p2),p2+1,p2);
    strcpy(line,tmp);
  }
}

void Adjust(void) {
  char tmp[STRMAX];

  if ((LEN==0) || (line[0]==';') || found("CODE_SEG") || found("END"))
    goto writeline;

  if (found(" DD ")) {
    sprintf(tmp,"%.*s DW 0,%s",(int)(p-line),line,p+4);
    strcpy(line,tmp);
  }

  if (isupper(line[0]) && (found(" DB ") || found(" DW "))) {
    (void)found(" "); p2=p; (void)found(" D");
    fprintf(incfile,"s_%.*s equ %c\n",(int)(p2-line),line,*(p+2));
    goto writeline;
  }

  finddel("[0]");

  if (found("LEA ")) {
    strncpy(p,"MOV ",4);
    sprintf(tmp,"%.*sOFFSET %s",(int)(strchr(line,',')+1-line),line,
            strchr(line,',')+1);
    strcpy(line,tmp);
  }

  if (!found(",O")) /* ",O[fF][sS][eE][tT]" */ {
    if (found("40:")) strncpy(p,"DS:",3);
    finddel("Word Ptr ");

    if (found("[") && (*(p+2) == ']') && isdigit(*(p+1))) {
      *p='_'; finddel("]");
    }

    fixplusbx(); fixop1(); fixop2(); incdec(); fixbrakdig();
  }

writeline:
  fprintf(newasm,"%s\n",line);
}

int main(int argc,char** argv) {
  oldasm=fopen((argc > 1) ? argv[1] : "INVADERS.ASM","r");

  if (oldasm == NULL) {
      fputs("\nFile not found!\n",stderr);
      return EXIT_FAILURE;
  }

  newasm=fopen("inv-wat.asm","w"); incfile=fopen(INCFILE,"w");

  fprintf(newasm,"B equ byte ptr\nW equ word ptr\ninclude %s\n",INCFILE);

  do {
    fgets(line,sizeof(line),oldasm);
    while ((LASTCHAR=='\n') || (LASTCHAR=='\r')) LASTCHAR='\0';
    if (feof(oldasm)) break;
    Adjust();
  } while (!feof(oldasm));

  fclose(incfile); fclose(newasm); fclose(oldasm);

  return EXIT_SUCCESS;
}

/* EOF */
