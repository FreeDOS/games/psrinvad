@echo off
:: Perl 5.8.8 is from 2006, but our DJGPP build is from 2007.
:: Unfortunately, that's the newest we have! And it's HUGE!
:: The full .ZIP (from DJGPP 2.04 /beta/) unpacked is 39 MB!
:: But it can be minimized (in a pinch), taking < 2 MB here.
::
:: NoMySo 4.3 (2009) by Michael Devore was meant to help old
:: code written for MASM/TASM to translate to NASM.
:: PSR Invaders (1995) was MASM syntax with implicit sizes, etc.

:begin
if "%PERLZIP%"=="" set PERLZIP=c:\zips\djgpp\perl588b
if "%NOMYSOZIP%"=="" set NOMYSOZIP=c:\zips\asm\nomyso43
if "%INVADERSZIP%"=="" set INVADERSZIP=c:\zips\asm\invadr11

:getperl
if not exist perl.exe unzip -Cjqn %PERLZIP% *\perl.exe
if not exist perl.exe goto end
set P0=warnings.pm strict.pm constant.pm carp.pm exporter.pm
set P1=s2p vars.pm integer.pm symbol.pm
for %%a in (%P0% %P1%) do if not exist %%a unzip -Cjqn %PERLZIP% *\%%a
set W0=warnings
if not exist %W0%\register.pm unzip -Cjqn %PERLZIP% *\register.pm -d %W0%
for %%z in (W0 P1 P0) do set %%z=

:getnomyso
if not exist nomyso.pl unzip -Cjqn %NOMYSOZIP% *.pl
if not exist nomyso.pl goto end

:getasm
if not exist invaders.asm unzip -Cjqn %INVADERSZIP% *.ASM
if not exist invaders.asm goto end

:sedfixes
set P2=perl -ne "s/@/\n/g;print"
echo /,O/s/\[0\]//@/LEA /{@s//MOV /@s/,/,OFFSET /@}@s/40://@| %P2% >s1
echo /^\.\.start/s/^/;/@/RemoveNewInt9:/,/CLC/s/\[\([^0]\)/[cs:\1/@| %P2% >s2
set P2=

:sed2perl
for %%a in (1 2) do perl s2p -f s%%a >s%%a.pl

:perl
perl s1.pl invaders.asm >inv1.as_
perl nomyso.pl inv1.as_ inv2.as_
del inv1.as_ >NUL
perl s2.pl inv2.as_ >inv.asm
del inv2.as_ >NUL

:nasm
nasm -o inv.com inv.asm -O3
echo.
echo INV.COM FFF22EF9
crc32 inv.com

:end
for %%z in (PERLZIP NOMYSOZIP INVADERSZIP) do set %%z=
