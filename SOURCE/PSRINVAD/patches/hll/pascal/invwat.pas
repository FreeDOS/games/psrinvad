{language=turbo}{AnsiC=1}
program invwat; {public domain, nenies proprajho, free for any use}
  type str=string[150];
  var line:str; p,p2:byte; oldasm,newasm,incfile:text;

procedure adjust;

  procedure dec(var i:byte); begin i:=i-1 end;
  procedure inc(var i:byte); begin i:=i+1 end;

  function len:byte;
  begin len := length(line) end;

  function copystr(ofs,num:integer):str;
  begin copystr := copy(line,ofs,num) end;

  function find(s:str):byte;
  begin find := pos(s,line) end;

  procedure ins(s:str;n:integer);
  begin insert(s,line,n) end;

  procedure del(ofs,num:integer);
  begin delete(line,ofs,num) end;

  function found(s:str):boolean;
  begin found := find(s) <> 0 end;

  procedure finddel(s:str);
  begin p := find(s); if p <> 0 then del(p,length(s)) end;

  procedure sub(old,new:str);
  begin p := find(old);
    if p <> 0 then begin del(p,length(old)); ins(new,p) end
  end;

  procedure incdec;
  begin p := find('INC '); if p=0 then p := find('DEC ');
    if p <> 0 then begin p:=p+length('??C ');
      while line[p]=' ' do inc(p);
      if line[p] <> '[' then begin p2 := p;
        repeat inc(p2) until (p2=len) or (line[p2]=' ') or
          (not ((line[p2] in ['a'..'z','A'..'Z']) or
                (line[p2] in ['0'..'9','+'])));
        if line[p2] in [' ','['] then dec(p2);
        if ((p2-p+1) > length('DX')) then begin
          ins(']',p2+1);
          ins('s_' + copystr(p,p2-p+1) + '[',p)
        end
      end
    end
  end;

  procedure fixop1;
  begin p2 := find(',');
    if p2 <> 0 then begin
      dec(p2); p := p2;
      if line[p2]=']' then begin
        repeat dec(p2) until line[p2]='[';
        dec(p2)
      end;
      repeat dec(p) until (line[p]=' ') or
        (not ((line[p] in ['a'..'z','A'..'Z']) or
              (line[p] in ['0'..'9','+','_','[',']'])));
      inc(p);
      if ((p2-p+1) > length('DX')) and (line[p] <> '[') and
        (line[p] in ['A'..'Z']) then begin
          ins(']',find(','));
          ins('s_' + copystr(p,p2-p+1) + '[',p)
      end
    end
  end;

  procedure fixop2;
  begin p := find(','); p2 := find(';');
    if (p <> 0) then if ((p2=0) or (p2 > p)) and (line[p] <> '[') then begin
      inc(p); p2 := p;
      while (p2 < len) and (line[p2] <> ' ') and
        ((line[p2] in ['a'..'z','A'..'Z']) or
         (line[p2] in ['0'..'9','+','_'])) do
          inc(p2);
      if line[p2] in [' ','['] then dec(p2);
      if (p2 > p) and (p2-p+1 > length('DX')) and (line[p] in ['A'..'Z'])
        then begin
          ins(']',p2+1);
          ins('s_' + copystr(p,p2-p+1) + '[',p)
      end
    end
  end;

label print;

begin
  if (len=0) or (line[1]=';') or found('CODE_SEG') then goto print;
  sub(' DD ',' DW 0,');

  if (found(' DB ') or found(' DW ')) then begin
    p := find(' '); p2 := p; while line[p2]=' ' do inc(p2);
    if p <> 1 then
      writeln(incfile,'s_',copystr(1,p-1),' equ ',line[p2+1]);
    goto print
  end;

  if found('LEA ') then begin sub(',',',OFFSET '); sub('LEA ','MOV ') end;

  if not found(',O') then begin
    sub('40:[','[DS:'); sub('ES:[','[ES:'); finddel('Word Ptr ');

    if found('+BX]') then begin
      p := find('[')+1; p2 := find('+')-1;
      sub('[','s_'+copystr(p,p2-p+1)+'[')
    end;

    fixop1; fixop2; incdec
  end;

print:
  writeln(newasm,line)
end;

begin
  assign(oldasm, 'INVADERS.ASM'); reset(oldasm);
  assign(incfile,'inv-wat.inc');  rewrite(incfile);
  assign(newasm, 'inv-wat.asm');  rewrite(newasm);

  writeln(newasm,'B equ byte ptr');  writeln(newasm,'W equ word ptr');
  writeln(newasm,'include inv-wat.inc');

  while not eof(oldasm) do begin readln(oldasm,line); adjust end;

  close(incfile); close(newasm); close(oldasm)
end.
