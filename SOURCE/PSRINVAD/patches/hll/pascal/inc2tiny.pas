{language=turbo}{AnsiC=1}
program inc2tiny; {public domain, nenies proprajho, free for any use}
const maxbytenames=60; maxbytenamelen=19;
type  str=string[150];
var   bytenames:array [1..maxbytenames] of string[maxbytenamelen];

procedure dec(var i:byte); begin i:=i-1 end;
procedure inc(var i:byte); begin i:=i+1 end;

procedure getbytenames;
const num:byte=1;
var line:string[31]; nasminc:text;
begin
  assign(nasminc,'inv-nasm.inc'); reset(nasminc);
  while not eof(nasminc) do begin
    readln(nasminc,line);
    if line[length(line)]='B' then begin
      bytenames[num] := copy(line,11,length(line)-12);
      inc(num)
    end
  end;
  close(nasminc)
end;

function isbytename(s:str):boolean;
var n:byte; done:boolean;
begin
  done := false; n := 1;
  while (n <= maxbytenames) and (not done) do begin
    if bytenames[n] = s then done := true;
    inc(n)
  end;
  isbytename := done
end;

procedure writenew;
type attrtype=(hasdigit,hasbrak,hascolon,hascomma);
var line:str; p,p2:byte; attr:set of attrtype;

  function len:byte; begin len := length(line) end;

  function cpy(ofs,num:integer):str; begin cpy := copy(line,ofs,num) end;

  function find(s:str):byte; begin find := pos(s,line) end;

  function found(s:str):boolean; begin p := find(s); found := p <> 0 end;

  procedure ins(s:str;n:integer); begin insert(s,line,n) end;

  procedure del(ofs,num:integer);
  begin if ofs > 0 then delete(line,ofs,num) end;

  procedure sub(older,newer:str);
  begin if found(older) then begin del(p,length(older)); ins(newer,p) end
  end;

  procedure fixhexnums;
  var hexbegin,hexend:byte;
    function foundhexnum:boolean;
    label 9;
    var y,z:byte;
    begin foundhexnum := false;
      for y := 1 to len do
        if (line[y] in ['0'..'9']) and (y < len) then begin
          hexbegin := y; z := y+1;
          while (line[z] in ['0'..'9','a'..'f','A'..'F']) and (z < len) do
            inc(z);
          if line[z]='h' then begin
            hexend := z-1;
            foundhexnum := true; goto 9
          end
        end;
    9:
    end;
  begin
    while foundhexnum do begin del(hexend+1,1); ins('0x',hexbegin) end
  end;

  procedure getattr;
  var i:byte;
  begin attr := []; i := 1;
    while (i <= len) and (line[i] <> ';') do begin
      case line[i] of
        '0'..'9': attr := attr + [hasdigit];
        '[': attr := attr + [hasbrak];
        ':': attr := attr + [hascolon];
        ',': attr := attr + [hascomma];
      else
      end;
      inc(i)
    end
  end;

var oldasm,newasm:text;
const myseg:string[3]='?S ';

begin
  assign(oldasm,'inv-nasm.asm'); reset(oldasm);
  assign(newasm,'inv-tiny.asm'); rewrite(newasm);

  while not eof(oldasm) do begin
    readln(oldasm,line); if len > 0 then getattr;

    if (len > 0) then if (line[1] <> '%') then begin
      if line[1] <> ';' then begin
        if (hasdigit in attr) then fixhexnums;

        if (hasbrak in attr) then
          if found('+BX') then begin
            del(p,3);
            repeat dec(p) until line[p]='[';
            ins('BX+',p+1)
          end;

        if ([hascolon,hasbrak] <= attr) then
          if found(':') then if (upcase(line[p-1])='S') and (line[p-3]='[')
               and (upcase(line[p-2]) in ['C','D','E','S'])
          then begin
            myseg[1] := upcase(line[p-2]); del(p-2,3); ins(myseg,1)
          end;
  
        if (hascomma in attr) then begin
          sub('LEA ','MOV ');
          if found(',O') and (upcase(line[p+2])='F') then del(p+1,7)
        end;

        if (hasbrak in attr) then
          if found('s_') then begin
            p2 := find('[');
            if isbytename(cpy(p+2,p2-p-2)) then
              sub(cpy(p,p2-p),'byte')
            else
              sub(cpy(p,p2-p),'word')
          end;

        if (line[1] in ['A'..'Z']) and (found(' DB ') or found(' DW ')) then
          ins(':',find(' '));

        if ([hascomma,hasbrak] <= attr) then
          if found(',AL') or found(' AL,') then sub('byte[','[')
          else if found(',AX') or found(' AX,') then sub('word[','[')
      end;

      writeln(newasm,line)
    end
  end;

  close(newasm); close(oldasm)
end;

begin
  getbytenames; writenew
end.
