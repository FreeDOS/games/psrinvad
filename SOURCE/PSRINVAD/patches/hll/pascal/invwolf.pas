{language=turbo}{AnsiC=1}
program invwolf; {public domain, nenies proprajho, free for any use}
  type str=string[150];
  var line:str; p:byte; oldasm,newasm:text;

procedure adjust;

  function len:byte;
  begin len := length(line) end;

  function copystr(ofs,num:integer):str;
  begin copystr := copy(line,ofs,num) end;

  procedure ins(s:str;n:integer);
  begin insert(s,line,n) end;

  procedure del(ofs,num:integer);
  begin delete(line,ofs,num) end;

  function found(s:str):boolean;
  begin p := pos(s,line); found := p <> 0 end;

  procedure finddel(s:str);
  begin if found(s) then del(p,length(s)) end;

  procedure sub(old,new:str);
  begin if found(old) then begin del(p,length(old)); ins(new,p) end
  end;

  procedure shorten;
  const jumpnum:byte=0;
  begin jumpnum := jumpnum + 1;
    if not (jumpnum in
      [3..6,12,14,17,19..21,23,24,30,36,39,42,45,47..70,89]) then
        sub('JMP ','JMPS ')
  end;

  procedure insertseg;
  const override:boolean=false;
  begin
    if found('RemoveNewInt9') and found(' PROC ') then override := true;
    if override and found('MOV ') then
      if found(',W') or found('StoreAX') or
        ((line[len-1]=',') and (line[len] in ['0','1'])) then
          writeln(newasm,' CS:');
    if found('CLC') then override := false
  end;

label print,adios;

begin
  if (len=0) or (line[1]=';') then goto print;

  if found('CODE_SEG') or found('END ') then goto adios;

  if found(' DB ') then while found('"') do sub('"','''');
  if found('SHL') or found('SHR') then finddel(',1');
  finddel(' Ptr'); finddel('40:');

  if found('[') then
    if (line[p+2]=']') and (line[p+1] in ['0'..'9']) then begin
      line[p] := '+';
      del(p+2,1)
    end;

  sub(' DD ',' DW 0,');

  if found(' ENDP') then del(1,p-1);

  if (line[1] in ['A'..'Z']) and found(':') then line[p] := ' ';

  if (found('CMP ') or found('MOV ') or found('INC ')) and
    (found('BombY') or found('BombT')) then
      sub('[','byte[');

  if found('ES:[') then begin sub('ES:[','['); writeln(newasm,' ES:') end;
  if found('LEA ') then begin sub('LEA ','MOV '); sub(',',',OFFSET ') end;

  if found('JMP ') then shorten; insertseg;

print:
  writeln(newasm,line);

adios:
end;

begin
  assign(oldasm,'INVADERS.ASM'); reset(oldasm);
  assign(newasm,'inv-wolf.asm'); rewrite(newasm);

  while not eof(oldasm) do begin readln(oldasm,line); adjust end;

  close(newasm); close(oldasm)
end.
