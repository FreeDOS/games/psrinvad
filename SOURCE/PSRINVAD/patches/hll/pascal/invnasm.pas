{language=turbo}{AnsiC=1}
program invnasm; {public domain, nenies proprajho, free for any use}
  type str=string[150];
  var line:str; p,p2:byte; oldasm,newasm,incfile:text;

procedure adjust;

  procedure dec(var i:byte); begin i:=i-1 end;
  procedure inc(var i:byte); begin i:=i+1 end;

  function len:byte;
  begin len := length(line) end;

  function copystr(ofs,num:integer):str;
  begin copystr := copy(line,ofs,num) end;

  function find(s:str):byte;
  begin find := pos(s,line) end;

  procedure ins(s:str;n:integer);
  begin insert(s,line,n) end;

  procedure del(ofs,num:integer);
  begin delete(line,ofs,num) end;

  function found(s:str):boolean;
  begin p := find(s); found := p <> 0 end;

  procedure finddel(s:str);
  begin if found(s) then del(p,length(s)) end;

  procedure sub(older,newer:str);
  begin if found(older) then begin del(p,length(older)); ins(newer,p) end
  end;

  procedure fixproc;
  begin
    if found(' PROC ') then begin
      repeat dec(p) until line[p] <> ' '; inc(p); line[p] := ':';
      del(p+1,len-p)
    end
  end;

  procedure fixop;
    procedure incdec;
    begin p := find('INC '); if p=0 then p := find('DEC ');
      if p <> 0 then begin p := p + length('??C ');
        while line[p]=' ' do inc(p);
        if line[p] <> '[' then begin p2 := p;
          repeat inc(p2) until (p2=len) or (line[p2]=' ') or
            (not ((line[p2] in ['a'..'z','A'..'Z']) or
                  (line[p2] in ['0'..'9','_'])));
          if line[p2]=' ' then dec(p2);
          if ((p2-p+1) > length('DX')) then begin
            ins(']',p2+1);
            ins('s_' + copystr(p,p2-p+1) + '[',p)
          end
        end
      end
    end;

    procedure fixop1;
    begin p2 := find(',');
      if p2 <> 0 then if line[p2-1] <> ']' then begin dec(p2); p := p2;
        repeat dec(p) until (line[p]=' ') or
          (not ((line[p] in ['a'..'z','A'..'Z']) or
                (line[p] in ['0'..'9','_'])));
        inc(p);
        if ((p2-p+1) > length('DX')) and (line[p] <> '[') and
          (line[p] in ['A'..'Z']) then begin
            ins(']',find(','));
            ins('s_' + copystr(p,p2-p+1) + '[',p)
        end
      end
    end;

    procedure fixop2;
    begin p2 := find(';');
      if found(',') then
      if ((p2=0) or (p2 > p)) and (line[p] <> '[') then begin
        inc(p); p2 := p;
        while (p2 < len) and (line[p2] <> ' ') and
          ((line[p2] in ['a'..'z','A'..'Z']) or
           (line[p2] in ['0'..'9','_'])) do
            inc(p2);
        if line[p2]=' ' then dec(p2);
        if (p2 > p) and (p2-p+1 > length('DX')) and (line[p] in ['A'..'Z'])
          then begin
            ins(']',p2+1);
            ins('s_' + copystr(p,p2-p+1) + '[',p)
        end
      end
    end;

    procedure fixplusbx;
    begin
      if found('+BX') then begin
        p := find('[')+1;
        sub('[','s_'+copystr(p,find('+')-p)+'[')
      end
    end;

  begin fixop1; fixop2; incdec; fixplusbx
  end;

  procedure unbrakdig;
  begin
    if found(']') then
    if (line[p-2]='[') and (line[p-1] in ['0'..'9']) then begin
      line[p-2] := '_'; del(p,1)
    end
  end;

  procedure fixbrakdig;
    function brakdig:boolean;
    begin brakdig := false;
      if (p <> 0) then
        brakdig := (line[p-2]='_') and (line[p-1] in ['0'..'9'])
    end;
  begin
    if found('[') and brakdig then del(p-2,2);
    if found(']') and brakdig then line[p-2] := '+'
  end;

  procedure fixseg;
  const insertseg:boolean=false;
  begin
    if found('RemoveNewInt9:') then insertseg := true;
    if insertseg and not found('[0') then sub('[','[cs:');
    if found('CLC') then insertseg := false
  end;

label print,adios;

begin
  if (len=0) or (line[1]=';') or found('LEA ') then goto print;
  sub(' DD ',' DW 0,');

  if (found(' DB ') or found(' DW ')) then begin
    p := find(' '); p2 := p; while line[p2]=' ' do inc(p2);
    if p <> 1 then
      writeln(incfile,'%define s_',copystr(1,p-1),' ',line[p2+1]);
    goto print
  end;

  if found('CODE_SEG') or found('END') then goto adios;

  finddel('[0]');

  if not found(',O') { ',O[fF][fF][sS][eE][tT]' } then begin
    sub('ES:[','[ES:'); finddel('40:'); finddel('Word Ptr ');

    fixproc; unbrakdig; fixop; fixbrakdig; fixseg
  end;

print:
  writeln(newasm,line);

adios:

end;

begin
  assign(oldasm, 'INVADERS.ASM'); reset(oldasm);
  assign(incfile,'inv-nasm.inc'); rewrite(incfile);
  assign(newasm, 'inv-nasm.asm'); rewrite(newasm);

  writeln(newasm,'%idefine offset'); writeln(newasm,'%define LEA MOV');
  writeln(newasm,'%define B byte');  writeln(newasm,'%define W word');
  writeln(newasm,'%include "inv-nasm.inc"');

  while not eof(oldasm) do begin readln(oldasm,line); adjust end;

  close(incfile); close(newasm); close(oldasm)
end.
