PSRINVAD 1.1L
=============

  * minor update
  + added
  - deleted
  ! functional change

* PSRINVAD.LSM
* BBSMODEM.TXT
* COVERAGE.TXT
* DISALL2.BAT
! P2C_WAT.MAK      (avoids getmem/freemem)
* README.TXT
* TODO.TXT
* INC2TINY.BAS
* INC2TINY.C
- BUGS.M2C
+ M2C.BUG          (renamed from BUGS.M2C)
* INVMOD2.TXT
* M2NASM.TXT
- INC2TINY.OB2     (moved into OBENASM.TXT)
! O2MYFILE.TXT     (added Oberon-M support)
! O2STR.TXT        (added Oberon-M support)
! OBENASM.TXT      (better modularity / library modules)
* FIXTURBO.TXT
* INC2TINY.PAS
! INVTURBO.TXT     (optionally uses less memory via getmem/freemem)
! ISONASM.TXT      (now also works with P4)
* INC2TINY.REX
* INV.REX
+ INVREX.SED       (make work with Quercus DOS demo)

<EOF>
